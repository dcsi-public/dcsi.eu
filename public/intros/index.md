---
title: Intros
motto: Automated Revolution
image: intros/agile_mind.jpg
#image: revomatico-solution-taskforce.jpg

#http://www.dcsi.eu/wp-content/uploads/2015/02/banner_2_dcsi-1020x372.png
image4: https://media.licdn.com/mpr/mpr/AAEAAQAAAAAAAAeeAAAAJDM4ZTk1Mjg2LTFhM2ItNDU0MC05YTUwLTE3YWM5NWZiOGY0YQ.png
height: 600px
image3: https://www.pivotaltracker.com/marketing_assets/agile/agile-methodolody-graphic-final-cleaned-38f492815647efae629a2f359c135972de4596b216cd0c32408e57d15f2fd3b4.png
image2: images/sliderimg.png
description: Help organizations understand and adopt new technologies.
titleForced: true
children:
  -
    title: Our domains
    image: Group 2.png
    description3: <i class="fas fa-cloud"></i>
    description: |
      <ul>
      <li><img src="https://material.io/tools/icons/static/icons/baseline-fingerprint-24px.svg">IT Security (Identity and Access Management)
      <li><img src="https://material.io/tools/icons/static/icons/baseline-device_hub-24px.svg">Systems integration, legacy and new
      <li><img src="https://material.io/tools/icons/static/icons/baseline-file_copy-24px.svg">Document Management
      <li><img src="https://material.io/tools/icons/static/icons/baseline-search-24px.svg">Data discovery, extraction and manipulation
      <li><img src="https://material.io/tools/icons/static/icons/baseline-description-24px.svg">Software development and continuous deployment
      <li><img src="https://material.io/tools/icons/static/icons/baseline-cloud-24px.svg">Private cloud
      </ul>
  -
    title: Our core philosophies
    image: Group 4.png
    description: |
      <ul>
      <li><img src="https://material.io/tools/icons/static/icons/baseline-playlist_play-24px.svg">Automation is key
      <li><img src="https://material.io/tools/icons/static/icons/baseline-mood-24px.svg">The User is focal
      <li><img src="https://material.io/tools/icons/static/icons/baseline-high_quality-24px.svg">Quality first
      <li><img src="https://material.io/tools/icons/static/icons/baseline-repeat-24px.svg">Continuously improve and deliver
      <li><img src="https://material.io/tools/icons/static/icons/baseline-device_hub-24px.svg">Integrate, migrate, adapt, refactor and decouple.
      <ul>
    description2: |
      <ul>
      <li><img src="https://material.io/tools/icons/static/icons/baseline-playlist_play-24px.svg">Automation is key: repetitive work is boring and time consuming for humans. So, let the computers do all the work.
      <li><img src="https://material.io/tools/icons/static/icons/baseline-mood-24px.svg">The User is focal: designing every new experience must be in service of the User, with better usability and enjoyment without sacrificing security and productivity.
      <li><img src="https://material.io/tools/icons/static/icons/baseline-high_quality-24px.svg">Quality first: while speed is very important, the quality is the most important factor for us. Rather than sacrificing the quality for quantity, we would change the technologies and the approach to bring the best quality faster.
      <li><img src="https://material.io/tools/icons/static/icons/baseline-repeat-24px.svg">Continuously improve and deliver: why wait for a couple of years for someone to create a big plan with everything in it before building? Start immediately with something smaller and of value, then continue adding on top of it at a fast and steady pace.
      <li><img src="https://material.io/tools/icons/static/icons/baseline-device_hub-24px.svg">Integrate, migrate, adapt, refactor and decouple.
      <ul>
---
