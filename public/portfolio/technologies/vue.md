---
title: Vue
titleHide: true
name: vue
image: http://blog.amio.io/content/images/2018/06/vue.png
description: "The Progressive JavaScript Framework
Vue (pronounced /vjuː/, like view) is a progressive framework for building user interfaces. Unlike other monolithic frameworks, Vue is designed from the ground up to be incrementally adoptable. The core library is focused on the view layer only, and is easy to pick up and integrate with other libraries or existing projects."
layout: listAll
#java skillset
sections:
    COMPONENTES:
        id: components
        image: https://www.roweb.ro/images/new_design/technologies/dotnet_page/web_app.png
        position: 1
        items:
        - MaterialDesign
        - Playframework
    DATA ACCESS:
        id: data-access
        image: https://www.roweb.ro/images/new_design/technologies/dotnet_page/data_access.png
        items:
        - JDBC
        - Spring JDBC
        - Hibernate ORM
        - JPA ORM
    SECURITY:
        id: security
        image: https://www.roweb.ro/images/new_design/technologies/dotnet_page/security.png
        items:
        - Spring Security
        - Encryption
        - Auditing
        - Logging
        - HTTPS/SSL
        - SSO - Single Sign-On
        - OAuth2/OpenId/SAML/PKI
    DATABASE:
        image: https://www.roweb.ro/images/new_design/technologies/dotnet_page/database.png
        items:
        - "[Microsoft SQL Server](https://www.roweb.ro/sql-server.html)"
        - MySql/MariaDb
        - PostgreSQL
        - Oracle Database
        - HSQL
        - IBM DB2
        - H2
        - SQLite

    WEB SERVICES:
        image: https://www.roweb.ro/images/new_design/technologies/dotnet_page/web_serv.png
        items:
        - Web Services
        - SOAP/XSD/WSDL
        - REST
        - Swagger

    UI COMPONENTS:
        image: https://www.roweb.ro/images/new_design/technologies/dotnet_page/ui_comp.png
        items:
        - bootstrap
        - GWT

    DESKTOP:
        image: https://www.roweb.ro/images/new_design/technologies/dotnet_page/desktop.png
        items:
        - SWING
        - SWT

    RIA:
        image: https://www.roweb.ro/images/new_design/technologies/dotnet_page/ria.png
        items:
        - Play Framework
        - JavaScript
        - "[JQuery](https://www.roweb.ro/jquery.html)"
avantaje:
    Modern technologies:
        items:
        - Scala
        - Play Framework
        - Swagger
        - Akka
        - Slick Db
        - Reactive Streams
        - Docker/Vagrant
        - Functional collections
        - Java8

    Web and Application Servers:
        items:
        - Tomcat
        - Jetty
        - JBoss
        - Weblogic

    "[State of the art tooling](https://zeroturnaround.com/rebellabs/java-tools-and-technologies-landscape-2016/)":
        items:
        - Maven
        - Jenkins
        - Sbt
        - Git

    Team with experience in:
        items:
        - Security
        - Identity Management
        - Scraping, Crawling, Indexing, Data Extractors
        - Distributed Flow Management, Event Sourcing, Message Exchange
        - Financial
        - Telecom
        - NMS

---


<img class="img-responsive" src="http://blog.amio.io/content/images/2018/06/vue.png"/>

Vue.js is more flexible, less straightforward in the solution. This allows you to structure your application as you want, rather than forcing it to do everything in Angular. Vue is just a presentation layer, so you can use it as an unobtrusive feature on the application page, instead of a full-scale framework. This gives you more opportunities for combining Vue with other libraries, but you are also responsible for more architectural solutions. For example, the Vue.js kernel does not include routing or ajax functions from the box, and usually assumes that you are building the application using an external module. This is probably the most important difference.



<!--
### Java/Scala Development

Java (and now Scala) ecosystem is one of the most stable and diverse where software solutions can flourish. The java technologies deliver core business in various industries from telecom to mobile, from web to enterprise solutions.   

[Home](https://www.roweb.ro/) ![>](file:///C:/work2/java-development/java-development_files/right_sign.jpg) [Technologies](https://www.roweb.ro/technologies) ![>](file:///C:/work2/java-development/java-development_files/right_sign.jpg) [Java Development](https://www.roweb.ro/technologies/java-development)

## WHAT WE DO DELIVER?

![brifcase](file:///C:/work2/java-development/java-development_files/brifcase.png)

#### FULL SERVICES

We analyze your business and offer tailor-made solutions and extended support. We have the solution to any type of requirement.

![tools](file:///C:/work2/java-development/java-development_files/tools.png)

#### TOP LEVEL EXPERTISE

Our extensive Java development experience and up-to-date training ensure the highest level of proficiency.

![star](file:///C:/work2/java-development/java-development_files/star.png)

#### QUALITY

We are using agile methodologies and practices including Test Driven Development that allow us to deliver exceptional high quality Java projects.

### Dedicated development teams

Our dedicated teams focus on one project at a time, so we can guarantee that each project gets the optimal skill set and attention. We select the perfect team depending on the technical, business and creative requirements, as well as the deadline and necessary amount of work.
-->
