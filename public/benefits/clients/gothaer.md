---
title: Gothaer
titleHide: true
name: Gothaer
image: gothaer.png
image3: https://www.gothaer.ro/wp-content/uploads/sigla.jpg
description: ""
---

# The client

The Gothaer Group is one of the largest insurance groups in Germany and one of the first nationwide mutual insurers in Germany with

- Almost 200 years of insurance expertise
- Strong focus on the individual needs of its clients and customers

Gothaer Versicherungsbank VVaG is the Gothaer Group parent-company, under whose umbrella act all other entities in the group:

- Gothaer Allgemeine AG
- Gothaer Lebensversicherung AG
- Gothaer Krankenversicherung AG
- Janitos Versicherung AG
- Gothaer Towarzystwo Ubezpieczen S.A.),
- Gothaer Asigurari Reasigurari

The Gothaer Group has been shaping the German insurance market with innovative solutions for almost 200 years,

- Offering non-life, life and health insurance products;
- Developing intelligent and flexible risk and financial concepts;
- Simple, yet powerful positioning: with flexible products and services that go beyond straight insurance, Gothaer is a particularly service-driven operator, individual problem solver and an efficient quick-response enterprise.

**A success story**

- Over 4.2 million insured members
- Gross premium income of over 4.4 billion euro, more than double of the entire Romanian insurance market
- Assets in excess of 28.9 billion euro
- Lines of business
    - Property & Casualty
    - Life
    - Health
    - Asset management
    - Risk management advisory

# The challenge

Migrating legacy systems to refreshed technologies: from Oracle to [[WSO2]].

<!--
# Achievements

# Value
-->
