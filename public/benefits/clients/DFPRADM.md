---
shortTitle: DFPRADM
title: Directorate For Persons Record And Databases Management
titleHide: true
name: Directorate For Persons Record And Databases Management
image: DFPRADM.png
description: ""
---

![](DFPRADM.png)

# The client

Directorate for Persons Record and Databases Management is a specialized unit of a Ministry of Administration and Interior. See <a href="http://depabd.mai.gov.ro/index_eng.html" target="_blank" rel="noopener">http://depabd.mai.gov.ro/</a>.

<!--
# The challenge

Migrating legacy systems to refreshed technologies: from Oracle to [[WSO2]].

# Achievements

# Value
-->
