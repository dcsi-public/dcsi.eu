---
title: European Central Bank
titleHide: true
image: EuropeanCentralBank.png
description: ""
---

![](EuropeanCentralBank.png)

# The client

The ECB is an official EU institution at the heart of the Eurosystem and the Single Supervisory Mechanism.
Over 2,500 staff from all over Europe work for the ECB in Frankfurt am Main, Germany. They perform a range of tasks in close cooperation with the national central banks within the Eurosystem and, for banking supervision, with the national supervisors within the Single Supervisory Mechanism.

<!--
# The challenge

Migrating legacy systems to refreshed technologies: from Oracle to [[WSO2]].

# Achievements

# Value
-->
