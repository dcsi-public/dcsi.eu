---
title: Contact
name: Contact
layout: page

footer: |
  <!-- Contact form JavaScript -->
  <!-- Do not edit these files! In order to set the email address and subject line for the contact form go to the bin/contact_me.php file. -->
  <script src="js/jqBootstrapValidation.js"></script>
  <script src="js/contact_me.js"></script>

commented: |
    @*
    <!-- Contact Form -->
    <!-- In order to set the email address and subject line for the contact form go to the bin/contact_me.php file. -->
    <div class="row">
        <div class="col-lg-8 mb-4">
            <h3>Send us a Message</h3>
            <form name="sentMessage" id="contactForm" novalidate>
                <div class="control-group form-group">
                    <div class="controls">
                        <label>Full Name:</label>
                        <input type="text" class="form-control" id="name" required data-validation-required-message="Please enter your name.">
                        <p class="help-block"></p>
                    </div>
                </div>
                <div class="control-group form-group">
                    <div class="controls">
                        <label>Phone Number:</label>
                        <input type="tel" class="form-control" id="phone" required data-validation-required-message="Please enter your phone number.">
                    </div>
                </div>
                <div class="control-group form-group">
                    <div class="controls">
                        <label>Email Address:</label>
                        <input type="email" class="form-control" id="email" required data-validation-required-message="Please enter your email address.">
                    </div>
                </div>
                <div class="control-group form-group">
                    <div class="controls">
                        <label>Message:</label>
                        <textarea rows="10" cols="100" class="form-control" id="message" required data-validation-required-message="Please enter your message" maxlength="999" style="resize:none"></textarea>
                    </div>
                </div>
                <div id="success"></div>
                <!-- For success/fail messages -->
                <button type="submit" class="btn btn-primary">Send Message</button>
            </form>
        </div>

    </div>
    <!-- /.row -->
    *@

---

<div class="row">
    <!-- Map Column -->
    <div class="col-lg-8 mb-4">
        <!-- Embedded Google Map -->
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2848.0380702285174!2d26.09163351608693!3d44.452890579102075!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40b1f8ac3d82c3dd%3A0x5c612e5602f7ffc5!2sDirect+Consulting+Services!5e0!3m2!1sen!2sus!4v1503432004542" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
    <!-- Contact Details Column -->
    <div class="col-lg-4 mb-4 vcard">
        <h3 class="fn">Revomatico</h3>
        <div class="mb-3 adr">
            <span class="street-address">Hatmanul Arbore 12</span>, <span class="locality">Bucharest</span> 
            <div class="extended-address">3rd floor</div>
            <div class="country-name">Romania</div>
        </div>
        <a class="email" title="Email" href="mailto:office@revomatico.com">office@revomatico.com</a>
        <div class="tel" title="Mobile">+40 743 011 172</div>
        <div class="tel" title="Phone">+40 348 401 305</div>
        <div class="hours mt-4" title="Open Hours">
            <div>Monday - Friday</div>
            <div>9:00 AM to 5:00 PM</div>
        </div>
    </div>
</div>
