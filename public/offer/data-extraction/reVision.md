---
title: reVision
shortTitle: reVision - cloud image recognition
subtitle: cloud image recognition
name: reVision
image: reVision.jpg
description: cloud image recognition
position: 1
layout: offer

offer:
  name: reVision
  slogan: cloud image recognition
  pitch: "Enable technology companies to use the best available OCR engine with reVision."
  description: "reVision will expose an API to be used by your clients to extract data from scanned resources: images, pdf to complete the digitalization."
  brochures:
    - 
      - name: reVision
      - type: pdf
      - link: https://drive.google.com/file/d/1flfBQ1KzdouBVHgSLJ8bPq6Zi3JVUE1E/view?usp=sharing
    - 
      - name: reVision
      - type: online demo
      - link: http://revision.revomatico.com
  benefits:
    - SaaS OCR - OCR as a Service
    - Easily demo the capabilities of the IRIS OCR engine
    - Scale easily to adapt to business spikes on OCR
    - Fast, accurate, big volumes
    - Easy integration of OCR On Premise or Cloud
    - Prepare for GDPR (search, retrieval) for already existing documents
    - Authentication, Rate-Limiting, Pay as you Go
    - The AI Engine will deliver faster, be more accurate and replace error prone, costly human resources
    - "AI Engine improves and become smarter: better technology, more trained, switch to better or specialized engine wile keeping the interface"
    - "Open OCR to startups markets: Mobile, web"
    
---

## Features

### Integration
- REST API
- Shared folders: synology, ftp, email
- Cloud folders: Dropbox/Google Drive/Box

### Data extraction
- Fully Unstructured: scan(jpg, png, tiff, pdf), mobile scan - extracted with
OCR, multi-language text recognition, barcode, matrix code,
classification
- PDF (All types of PDF files including multi-page PDFs)
- TIF/TIFF (Multipage TIFFs supported)
- Images: JPEG/JPG, BMP, PCX, PNG, GIF
- ZIP, emails
- On request Plotter PLT, HPGL, HPRTL, HPGL2, ECM, PostScript

### Operations
- Correction - crop, smart crop, filtering, rescanning
- Transform - consolidate, standardize, cleanse, reconcile data
- Automatic classification and clustering - invoices, timesheets, resumes
- Image similarities

### Sample Applications
- docLess - document management software
- Personal Data Scan - detection on scanned documents
- Find Document by Image - search similar pages in existing documents
by scanned image
- Autofix - AI based data correction
### Technical Features
- Run the OCR engine on containerized linux servers in headless mode
(non windows)
- Image/text/ocr simillarities
- Content automatic categorization and identification
- Engines: Tesseract4, future: IRIS
### Technology Stack
- Infrastructure: kong, kubernetes, docker
- Scala, Akka, Playframework, Java, Bootstrap4, Responsive