---
title: Access Management
titleForced: true
description: Central authentication to web & mobile applications, single sign-on, access management, strong authentication
image: https://www.beyondtrust.com/wp-content/uploads/diagram-identity-access-management.png
layout: folder
---

<img src="https://www.beyondtrust.com/wp-content/uploads/diagram-identity-access-management.png" class="img-fluid"/>

- Single Sign-On: achieve single sign-on to any SAML 2.0 or OpenID Connect protected application.
- Access Management: Using UMA, a profile of OAuth 2.0, your organization can secure API's and centralize authorization policies for applications.
- Multi-Factor Authentication: Configure multi-factor and multi-step authentication to applications, and call external API�s such as intrusion detection.
- Directory Integration: Bridge your existing identity infrastructure and your applications, and leverage user information across Active Directory or any LDAP V3 server.
- User Management: Add, edit and manage people, groups and user attributes and claims to ensure the proper information is released about the right people.
- Enrollment: Customize workflows relating to the enrollment and registration process people face when registering new accounts at new applications.

For Access Management, API Security and User Managed Access, we specialize in the following products:

- Keycloak
