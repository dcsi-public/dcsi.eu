---
title: GDPR
subtitle: General Data Protection Regulation
name: GDPR
image: gdpr.png
description: General Data Protection Regulation
position: 3
layout: offer

offer:
  name: GDPR
  slogan: General Data Protection Regulation
  pitch: "Analyze the GDPR impact on your business: initial inspection, implementation strategy, operation and audit."
  description: "General Data Protection Regulation (EU) 2016/679 started on 25 May 2018. It means that organizations should be able 
  to demonstrate that they have analyzed the GDPR requirements in relation to their processing of personal data and that they have 
  implemented a system or programme that allows them to achieve compliance."
  brochures:
    - 
      - name: gdpr executive summary
      - type: pdf
      - link: https://drive.google.com/open?id=1aPbow4XQcKAJO4eiwpaSwZXYYbXlJuRF
    - 
      - name: gdpr details
      - type: pdf
      - link: https://drive.google.com/open?id=1eJuyb1qSYhHj6LVcwsffOT00e-ZwoSzQ
  benefits:
    - Accurate customer data
    - Additional care on sensitive data
    - Minimize security incident
    - Minimize data breaches
    - Save money - The maximum fines for non-compliance could reach 20 million or 4% of the organisation;s worldwide turnover
  services:
    
---

## Services

### Initial Assesment

- Presentation - Intro to GDPR 
- Workshop - Information mapping and data audit 
    - Survey 
    - Interviews 
    - Documents 
        - Systems 
        - Procedures 
        - Personal information data flow 

- Delivered Report 
    - As-is Analysis 
    - Action Plan 

### Implementation

- Gap analysis 
- Information Architecture with focus on Personal Data 
- Technology selection 
- Document Management Systems - go paperless 
- Identity and Access Management - traceability, rights management 
- Data Transformation and Migration - anonymization, legacy systems upgrade 

### Operation

- Externalized or contained personal data with paperless and IAM systems 
- Assisting DPO activities: training, compliance, audits 
- Identity and Access Management - traceability, rights management 

### Audit

- Audit preparation 
- Data source identification 
- Data source inventory 
- Data source management 
- Audit results 

### Legal Assistance

- Avocat Cristina Radu

<!--
# Intro - Law

## Why is important
- Regulation - A "regulation" is a binding legislative act. It must be applied in its entirety across the EU. For example, when the EU wanted to make sure that there are common safeguards on goods imported from outside the EU, the Council adopted a regulation.
- The regulation was adopted on 27 April 2016. It becomes enforceable from 25 May 2018 after a two-year transition period and, unlike a directive, it does not require national governments to pass any enabling legislation, and is thus directly binding and applicable.
- Regulation (EU) 2016/679 of the European Parliament and of the Council of 27 April 2016 on the protection of natural persons with regard to the processing of personal data and on the free movement of such data, and repealing Directive 95/46/EC (General Data Protection Regulation) (Text with EEA relevance)

## Laws
- Directory at http://eur-lex.europa.eu/legal-content/en/TXT/?uri=CELEX%3A32016R0679
- Texts in english(html,pdf), romanian(html,pdf) and side by side(html) at  http://eur-lex.europa.eu/legal-content/EN-RO/TXT/?uri=CELEX:32016R0679&from=en
- nicely structured - https://gdpr-info.eu/

## Definitions
- Most of them in Article.4
- Personal data = date caracter personal
- DPO - Data Protection Officer = Responsabil Protectia Datelor
- Operator = Controller

# Intro - Non Legal Summary

## Know holded data

- Who are our data subjects? Who has access to sensitive data? 
- Where do we keep their personal data? Where do we transfer personal data to? 
- Why is personal data under our control (for what legitimate purpose)? Why do we share it with third parties? Do third parties share it with other entities? If so, who, how many and to what purpose? 
- When are we keeping personal data until? When do we share personal data with others? 
- What mechanisms do we have in place to safeguard personal data? 
- How is data being processed? How long should it be kept? 

## Rights of individuals (subjects)

- to be informed 
- to access 
- to rectification 
- to erasure 
- to restrict processing 
- to data portability 
- to object 
- not to be subject to automated decision-making including profiling. 

## Others

- Legal basis for processing 
- Notices & privacy communications 
- Managing consent 
- Data security & breaches 
- Data Protection Officer 
- Pseudonymisation 
- Sanctions 
- Data protection by Design and by Default 

## Legal Help

- Partners

# Analysis Directions

## Whom

- organization 
    - number of people 
    - number of systems 
    - organigram 
    - Roles 

- DPO - Responsabil Protectia Datelor 
- Top Down Roles Interviews 

## Environment

- External entities 
    - Organizations 
    - Systems 

- Roles 
    - internal 
    - external 

- Systems 
    - Physical - what data, how do you have access 
        - buildings 
        - rooms 
        - drawers 
        - keys 

    - Software & Data storage 
        - internal/external 
        - On Premise/Cloud 

- Procedures 

## Data Sets

- Medium: analogic/digital 
- Permissions: given, not given, unknwon 
- Subject: personal/non-personal 
- Why: Anonymized/Needed 

## Checks

- Who are our data subjects? Who has access to sensitive data? 
- Where do we keep their personal data? Where do we transfer personal data to? 
- Why is personal data under our control (for what legitimate purpose)? Why do we share it with third parties? Do third parties share it with other entities? If so, who, how many and to what purpose? 
- When are we keeping personal data until? When do we share personal data with others? 
- What mechanisms do we have in place to safeguard personal data? 
- How is data being processed? How long should it be kept?

-->