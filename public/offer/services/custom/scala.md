---
title: Scala
titleHide: true
name: scala
image: scala.png
description: "Scala builds on top of java ecosystem. With it you can develop faster using a modern dynamic style while functional concepts like imutability enable more robust, concurent and parallel solutions. And all these are tightly integrated and reusable from any java solution."
layout: listAll
sections:
    WEB APPLICATIONS:
        id: web-applications
        image: https://www.roweb.ro/images/new_design/technologies/dotnet_page/web_app.png
        items:
        - Playframework
    DATA ACCESS:
        id: data-access
        image: https://www.roweb.ro/images/new_design/technologies/dotnet_page/data_access.png
        items:
        - JDBC
        - Slick
        - Anorm
    SECURITY:
        id: security
        image: https://www.roweb.ro/images/new_design/technologies/dotnet_page/security.png
        items:
        - silhouette
        - Encryption
        - Auditing
        - Logging
        - HTTPS/SSL
        - SSO - Single Sign-On
        - OAuth2/OpenId/SAML/PKI
    DATABASE:
        image: https://www.roweb.ro/images/new_design/technologies/dotnet_page/database.png
        items:
        - Microsoft SQL Server
        - MySql/MariaDb
        - PostgreSQL
        - Oracle Database
        - HSQL
        - IBM DB2
        - H2
        - SQLite

    WEB SERVICES:
        image: https://www.roweb.ro/images/new_design/technologies/dotnet_page/web_serv.png
        items:
        - Web Services
        - SOAP/XSD/WSDL
        - REST
        - Swagger

---

![](scala.png)

