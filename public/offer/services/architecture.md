---
title: Architecture & Technology Selection
titleHide: true
image: http://vanaken-consulting.nl/wp-content/uploads/2015/08/Solution-Architecture.png
description: ""
position: 1
---

Architecture & capacity planning are important parts of environment design. When trying to implement a new application, a good architecture means fewer problems down the line.

This phase can also be performed as an architecture review when the environment already exists or before environment changes.

## This service includes

- Determine service level requirements: analyze the load done by every system and quantify users’ expectations of that system.
- Determine applicative needs and required application solutions.
- Identify high availability and business continuity needs – backup strategy, disaster recovery, clustering and cloud solutions.
- Capacity planning: storage and server sizing such as CPU requirements and memory usage.
- Analyze the current capacity and determine how it meets the needs of the users.
- Plan for the future: using forecasts of future business activity, design the future system requirements.
- Implement the required changes in system configuration will ensure that sufficient capacity will be available to maintain service levels, even as circumstances change in the future.

## Deliverables

The deliverable will include a document describing the current status and a suggested new architecture(s).

Lucruri importante!!!

- Environment architecture design
- High availability and disaster recovery solutions based on the actual needs and environment
- Applicative review in terms of integration needs

Architecture review and capacity planning for existing system will also include the current status in terms of CPU/Memory/Storage/Network capabilities along with suitable configuration changes to the application.

## Benefits

Proper capacity planning benefits the customer in significant, measurable ways by ensuring that IT capacity exists and that it is closely matched to the current and future identified needs of customer business. Having the correct architecture design and a good capacity planning ahead of time will save time and money in later stages of the system lifecycle.
