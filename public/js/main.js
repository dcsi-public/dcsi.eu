var $ = jQuery.noConflict();
$(document).ready(function () {
	"use strict";
	scrollUpBinding();
	showDevMode();
});

function scrollUpBinding(){
	"use strict";
	$('.scrollup').on('click', function(e){
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });
}

function showDevMode() {
//	if (window.location.href.indexOf("devmode") > -1 || (
//			(window.location.href.indexOf("://localhost") > -1 || window.location.href.indexOf("://127.0.0.1") > -1)) && (window.location.href.indexOf("devmode=false") == -1)) {
	   $('.dev').show();
//	}
}