import java.io.Closeable

import scala.concurrent.Await

lazy val dcsiSite = (project in file("."))
  .enablePlugins(SbtTwirl, SbtWeb)
name := "revo-site"
//lazy val dcsiJavaModule = (project in file("testModule")).dependsOn(dcsiSiteModule) 
val dev=false
scalaVersion := "2.12.6"
crossScalaVersions := Set(scalaVersion.value, "2.10.6", "2.11.11", "2.12.6", "2.13.0-M4").toSeq //to remove duplicates
//"2.10.6" - lightbend-markdown-server is not compiled for 2.10.x
// "com.lightbend.markdown" %% "sbt-lightbend-markdown" % "1.6.0" 
//scalacOptions += "-Ylog-classpath"

JsEngineKeys.engineType := JsEngineKeys.EngineType.Node
WebKeys.stagingDirectory := file("public")
cleanFiles += (baseDirectory { base => base / "public" }).value

resolvers += "raisercostin repository" at "http://dl.bintray.com/raisercostin/maven"

libraryDependencies ++= Seq(
  /*
      "org.webjars" % "normalize.css" % "3.0.2",
      "org.webjars" % "foundation" % "6.2.0",
      "org.webjars.bower" % "waypoints" % "4.0.0",
      "org.webjars" % "prettify" % "4-Mar-2013",
  */
  "com.typesafe.akka" %% "akka-http" % "10.0.13",
  //used in pages
  "org.webjars" % "font-awesome" % "4.7.0"
  , "org.webjars" % "bootstrap" % "4.0.0-beta" //"4.0.0-beta"
  , "org.webjars.npm" % "popper.js" % "1.11.1"
  , "org.webjars" % "tether" % "1.4.0"
  , "org.webjars" % "jquery" % "2.2.1" //3.2.1
  , "org.raisercostin" %% "jedi-io" % "0.60"
  , "org.raisercostin" %% "jedi-nodes" % "0.10"

  , "com.lightbend.markdown" %% "lightbend-markdown-server" % "1.6.1"
  , "junit" % "junit" % "4.12" % "test"

  , "com.vladsch.flexmark" % "flexmark-all" % "0.26.4" //to use only core without dependencies?

  , "ch.qos.logback" % "logback-classic" % "1.2.3"
  , "org.slf4j" % "slf4j-api" % "1.7.5"
  , "io.projectreactor" % "reactor-scala-extensions_2.12" % "0.3.5"
  ,"com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.9.6"
)
libraryDependencies += "org.json" % "json" % "20170516"
//libraryDependencies += "com.github.pathikrit" %% "dijon" % "0.2.4"
//libraryDependencies += "com.propensive" %% "rapture-json-jackson" % "2.0.0-M8"
libraryDependencies += "com.propensive" %% "rapture-json-spray" % "2.0.0-M8"

libraryDependencies ++= Seq(
  "org.springframework.boot" % "spring-boot-starter" % "2.0.1.RELEASE",
  "org.springframework.boot" % "spring-boot-starter-data-rest" % "2.0.1.RELEASE",
  "org.springframework.boot" % "spring-boot-starter-webflux" % "2.0.1.RELEASE",
  "org.springframework.boot" % "spring-boot-devtools" % "2.0.1.RELEASE",
  "com.typesafe" % "config" % "1.3.0"
)

resolvers += Resolver.bintrayIvyRepo("typesafe", "ivy-releases")
val publicVersion = "0.3"
val assetFingerPrint = publicVersion //"git rev-parse HEAD".!!.trim

val httpServer = AttributeKey[Closeable]("http-server")

val stopCommand = Command.command("stop") { state =>
  state.attributes.get(httpServer) match {
    case Some(server) =>
      server.close()
      state.remove(httpServer)
    case None => state
  }
}

/*
val runCommand = Command.command("run") { state =>
  val task1 = (runMain in Compile).toTask(Seq(
      "org.raisercostin.sekyll.SekyllSpringBootMainServer"
    ).mkString(" ", " ", ""))

    Project.runTask(task1, state)
  state
}
*/


val runCommand = Command.make("run") { state =>
  import complete.Parsers._
  import complete.Parser
  import scala.concurrent.duration._

  (Space ~> NatBasic).?.map { maybePort =>
    () =>
      val port = maybePort.getOrElse(8080)

      val log = state.log
      val extracted = Project.extract(state)

      val stageDir = extracted.get(WebKeys.stagingDirectory)

      log.info(s"\u001b[32mRunning HTTP server on http://localhost:$port, press ENTER to exit...\u001b[0m")
      val simpleHttpServer = SimpleHTTPServer(stageDir, port)
      Await.ready(simpleHttpServer.bindingFuture, 5.seconds)

      val stateWithStop = "stop" :: state.put(httpServer, new Closeable {
        override def close(): Unit = {
          log.info("Shutting down HTTP server")
          simpleHttpServer.close()
        }
      }).addExitHook(() => simpleHttpServer.close())

      val extraSettings = Seq(
        javacOptions ++= Seq("-source", "1.8"),
        javaOptions += (if(dev) "-Ddev" else ""),
        fork := true // required for javaOptions to take effect
        //fork := false //needed for debug
      )
      val stateWithExtraSettings = extracted.append(extraSettings, stateWithStop)
      Parser.parse("~web-stage", stateWithExtraSettings.combinedParser) match {
        case Right(cmd) => cmd()
        case Left(msg) => throw sys.error(s"Invalid command:\n$msg")
      }
  }
}

val deploySiteCommand = Command.args("deploySite", "deploy staged site to master branch") { (state, args) =>
  println("start deploy")
  /*
  //val state2 = Command.process("clean",state)
  //val state3 = Command.process("web-stage",state2)
  val state3 = state
  val remote = "origin"
  println(System.getProperty("java.class.path"))
  println(System.getenv("CLASSPATH"))
  val deployBranch = "master"
  println("hi header")
  "cmd /c echo hi".!!
  println("git")
  "cmd /c git status".!!
  println("done")
  
  val commands = s"""
  git status
  """
  
  echo git init;
  echo git add .
  echo commit -m "Website build"
  # Push the repo to the master branch of the main repo
  echo git push ../../.. master:$deployBranch -f

  # Push the repo to the website
  cd ../../..
  echo git push $remote $deployBranch:master -f 
  """
  
  println(s"running [$commands]")
  commands.!!
  state3
  */
  state
}

commands ++= Seq(runCommand, stopCommand, deploySiteCommand)

val generateHtml = taskKey[Seq[File]]("Generate the site HTML")

target in generateHtml := WebKeys.webTarget.value / "generated-html"
generateHtml := {
  Def.taskDyn {
    val outputDir = (target in generateHtml).value
    Def.task {
      (runMain in Compile).toTask(Seq(
        "org.raisercostin.sekyll.SekyllMainFromBuild",
        outputDir,
        assetFingerPrint
      ).mkString(" ", " ", "")).value
      outputDir.***.filter(_.isFile).get
    }
  }
}.value

def path(segments: String*): String = segments.mkString(java.io.File.separator)

Concat.groups := Seq(
  s"css/all-$assetFingerPrint.css" -> group(Seq(
    path("lib", "tether", "css", "tether.css"),
    path("lib", "bootstrap", "css", "bootstrap.css"),
    path("lib", "font-awesome", "css", "font-awesome.css"),
    path("css", "style.css"),
    path("css", "main.css")
  )),
  s"js/all-$assetFingerPrint.js" -> group(Seq(
    path("lib", "jquery", "jquery.js"),
    path("lib", "popper.js", "dist", "umd", "popper.js"),
    path("lib", "tether", "js", "tether.js"),
    path("lib", "bootstrap", "js", "bootstrap.js"),
    path("js", "main.js"),
    path("js", "scripts.js"),
    path("js", "contact_me.js"),
    path("js", "jqBootstrapValidation.js")
  ))
  /*
  ,
  s"$assetFingerPrint-all-styles-concat.css" -> group(Seq(
      path("lib", "foundation", "dist", "foundation.min.css"),
      path("lib", "prettify", "prettify.css"),
      "main.min.css"
  )),
  s"$assetFingerPrint-all-scripts-concat.js" -> group(Seq(
    path("lib", "jquery", "jquery.min.js"),
    path("lib", "foundation", "dist", "foundation.min.js"),
    path("lib", "waypoints", "lib", "jquery.waypoints.min.js"),
    path("lib", "waypoints", "lib", "shortcuts", "sticky.min.js"),
    path("lib", "prettify", "prettify.js"),
    path("lib", "prettify", "lang-scala.js"),
    "main.min.js"
  ))
  */
)

//StylusKeys.compress := true
JsEngineKeys.engineType := JsEngineKeys.EngineType.Trireme

pipelineStages := Seq(uglify, concat)
WebKeys.pipeline ++= {
  generateHtml.value pair relativeTo((target in generateHtml).value)
}
watchSources ++= {
  ((sourceDirectory in Compile).value / "markdown").***.get ++
    (baseDirectory.value / "site").***.get ++
    (baseDirectory.value / "site" / "_layouts").***.get ++
    (baseDirectory.value / ".sekyll.yml").get ++
    (baseDirectory.value / "src" / "main" / "public").***.get
}

import play.twirl.sbt.Import.TwirlKeys._

sourceDirectories in(Compile, compileTemplates) += baseDirectory.value / "site" / "_layouts"
//sourceDirectories in compileTemplates += baseDirectory.value / "site" / "_layouts"
//sourceDirectories in twirlCompileTemplates <<= file("site" / "layouts")
//compile:twirlCompileTemplates::sourceDirectories


compileOrder in Compile := CompileOrder.Mixed
compileOrder in Test := CompileOrder.Mixed
