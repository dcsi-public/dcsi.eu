# Site todo
- add GPDR - http://www.eugdpr.org/the-regulation.html

The sample site generated in `master` branch: http://raisercostin.org/sekyll

A second sample is at http://raisercostin.gitlab.io/sekyll

# Sekyll


## How it works

The content is generated in `docs/` folder.
Steps:
  - there are several types of files that will be statical types
    - **/*.md - markdon files converted to html
    - `src/main/twirl` - layout/tags pages that are compiled
       - any page can refer the entire content via `@item.site.xxx` properties
    - all other files - copied directly in docs folder

Everything in `docs/` is published at raisercostin.gitlab.io .
The content in `docs` is a manual copy of a `target/web/stage`.

Publish on
- github.com - See https://help.github.com/articles/configuring-a-publishing-source-for-github-pages/

## Generating site locally

The build is made with `sbt`.
- eclipse: `sbt eclipse`
- compile: `sbt clean compile`
- generate web: `sbt webStage`
- start server for in dev mode with autorefresh: `sbt run` . Then go on http://localhost:8080/
- start server in debug mode `activator -jvm-debug 5005 "run 8080"`

# Features
- types of urls - see https://www.digitalocean.com/community/tutorials/controlling-urls-and-links-in-jekyll for more explanations.
  - absolute: starting with `http://`, `https://` - These urls will not be changed at all.
  - root relative: starting with `/` - These urls will be prefixed with the configured context if specified.
  - relative: any other urls - These urls will be prefixed with configured context if specified and a relative path to the place that contains the content.
- serialize scala case classes in spring boot rest with jackson
- add scala config https://github.com/andr83/scalaconfig
  
## History

### 2017-08-18
- upgrade to scala 2.12 (better integration with java 8 lambdas)

### 2017-09-16
- add yaml integration
  - https://en.wikipedia.org/wiki/YAML - nice samples
  - https://github.com/jcazevedo/moultingyaml
  - https://learnxinyminutes.com/docs/yaml/
  - https://bitbucket.org/asomov/snakeyaml/wiki/Documentation
- add import from freemind (.mm) files
  - http://freemind.sourceforge.net/wiki/index.php/Import_and_export
- add dev mode
  - https://stackoverflow.com/questions/3162457/how-to-check-with-javascript-if-connection-is-local-host

### 2017-08-29
- add sitemap generation

### 2017-08-26
- add logo
- add active menus
- add title
- generat favicons - http://www.favicon-generator.org/

### 2017-08-11
- more control of assetFingerPrint:
  1. manually changed in the `publicVersion` value in `build.sbt`? Drawback: browsers might cache styles and javascripts for long time and will not reload new versions. The generator should generate a new file from time to time or when the resources are changed.

# Backlog

## ToDo
- list 400, 500 page errors at the end
- generate breadcrumb
- add i18n as in https://github.com/untra/polyglot (and for a list see https://github.com/Anthony-Gaudino/jekyll-multiple-languages-plugin)
- detect local broken links (from markdowns)
- add multiple source folders
- add dynamic filtering: https://vestride.github.io/Shuffle/
- publish at http://sekyll.gitlab.io and http://sekyll.github.io
- generate directly in `docs/` not in `target/web/stage`
- switch from style and sass to less as less has native compilation in sbt.
- cleanup
- deploy command to release static site to
  - folder
  - other branch
  - via travis
    - https://gist.github.com/domenic/ec8b0fc8ab45f39403dd
    - https://benlimmer.com/2013/12/26/automatically-publish-javadoc-to-gh-pages-with-travis-ci/
    - https://www.google.ro/search?q=github+publish+gitpages+after+travis
- render current branch by scripts in
  - github
  - gitlab (for private repos)
- yaml inheritance with default config
  - https://web.archive.org/web/20130213112648/http://blog.101ideas.cz/posts/dry-your-yaml-files.html
- publish via travis: https://github.com/jostly/template-sbt-travis
- swtich tags to semantic tags
  - https://coderwall.com/p/wixovg/bootstrap-without-all-the-debt
  - https://css-tricks.com/semantic-class-names/
  - https://stackoverflow.com/questions/22133639/bootstrap-and-html5-semantic-tags
  - https://stackoverflow.com/questions/23583235/how-to-combine-twitter-bootstrap-3-and-html5-semantic-elements
  - https://www.w3schools.com/html/html5_semantic_elements.asp
        Tag	Description
        <article>	Defines an article
        <aside>	Defines content aside from the page content
        <details>	Defines additional details that the user can view or hide
        <figcaption>	Defines a caption for a <figure> element
        <figure>	Specifies self-contained content, like illustrations, diagrams, photos, code listings, etc.
        <footer>	Defines a footer for a document or section
        <header>	Specifies a header for a document or section
        <main>	Specifies the main content of a document
        <mark>	Defines marked/highlighted text
        <nav>	Defines navigation links
        <section>	Defines a section in a document
        <summary>	Defines a visible heading for a <details> element
        <time>	Defines a date/time
- render first requested page and continue with others in background (to speedup dev lifecycle)
- add `@item.site.page` context info about current page. Two ways to implement it:
  - the iterator should modify this.
  - the Site is actually a Page that refers the site.  
- strict/lax evaluation of meta parameters 
- fnmatch (gitignore pattern format) implementation in scala 
  - https://gist.github.com/Aivean/6b2bb7c2473b4b7e1376fac1d2d49cf8
  - https://www.google.com/search?q=fnmatch+implemenataion+in+scala
- detect changed markdown files
- change markdown from pegdown to kramdown
- add mega menu https://bootsnipp.com/snippets/featured/bootstrap-mega-menu
- add markdown import/export - http://freemind.sourceforge.net/wiki/index.php/Import_and_export
- include pages from markdown
- iterators in markdown?
- semantic
  - https://nikcodes.com/2013/08/20/semantic-markdown/
  - https://blog.codinghorror.com/the-future-of-markdown/
  - http://markdown.github.io/
  - http://thenewcode.com/536/Adding-Phone-Numbers-To-Web-Pages-With-HTML5-and-Microdata
  - https://www.w3.org/TR/html5/common-idioms.html#conversations
  - http://microformats.org/wiki/hcard - hCard is a simple, open format for publishing people, companies, organizations on the web, using a 1:1 representation of vCard (RFC2426) properties and values in HTML. hCard is one of several open microformat standards suitable for embedding data in HTML/HTML5, and Atom/RSS/XHTML or other XML.
- content over form
  - https://www.crazyegg.com/blog/landing-page-essentials/
- check with web tools
  - pwa - Progressive Web App Checklist
    - https://developers.google.com/web/progressive-web-apps/
    - checklist & tools - https://developers.google.com/web/progressive-web-apps/checklist
  - AMP - Accelerated Mobile Pages 
    - https://en.m.wikipedia.org/wiki/Accelerated_Mobile_Pages
    - https://www.ampproject.org/
    - https://developers.google.com/amp/
    - amp html spec - https://www.ampproject.org/docs/fundamentals/spec
    - amp toolbox - https://github.com/ampproject/amp-toolbox
  - webmaster tools
    - https://www.google.com/webmasters/tools/mobile-usability?hl=en&siteUrl=http://www.dcsi.eu/#drilldown=CONTENT_NOT_SIZED_TO_VIEWPORT
    - https://developers.google.com/web/tools/lighthouse/audits/first-meaningful-paint
  - lighthouse
    - rec by https://developers.google.com/web/progressive-web-apps/checklist
    - extension https://chrome.google.com/webstore/detail/lighthouse/blipmdconlkpinefehnmjammfjpmpbjk
    - Quick-start guide on using Lighthouse:
      - http://bit.ly/lighthouse-quickstart 
      - https://docs.google.com/document/d/1pwLP9t8gKB3XyNKKz15dUPhWOeuHSqJ01paF9lN_y5g/edit
  - google
    - search console
      - https://www.google.com/webmasters/tools/site-message-view?hl=en&authuser=0&siteUrl=http://www.dcsi.eu
    - custom search 
      - https://cse.google.com/cse/create/new?hl=en&cselang=en&utm_source=wmx
  - favicon
    - https://realfavicongenerator.net/

## Bugs
- hover doesn't work on tablets/devices

## How could work
- Generate in a separate branch and publish from there.
  - There are some issues on keeping the history of that.
- Uploading only the sources and generate the public folder at runtime has the drawback of not having the history of the final generated site.

## Quircks
Here we explain what we tried and failed.

### Gitlab
- Don't publish the `target/web/stage` folder directory by configuring it in `.gitlab-ci.yml` as this will fail.
- Sass and Style sbt-web plugins need external dependency on nodejs/npm and it will take longer time to compile.
- Sbt fails to build in gitlab after 1h.

## Rejected
- more control of assetFingerPrint:
  - based on the git head: `val assetFingerPrint = "git rev-parse HEAD".!!.trim`
     - This has the problem that on any change anywhere all the files are affected.
  - based on the actual content of each generated/combined file instead of current version in head.
     - This is a fingerprinting on each file. Might work if a fingerprint on content on all of the files is computed.

# Resourcs

## Bootstrap templates/themes/style

- https://wrapbootstrap.com/theme/raleway-mega-bootstrap-template-WB09054T6
- https://startbootstrap.com/template-overviews/modern-business/


# Old lagom site
## Lagom website

This project is responsible for generating the Lagom website.

The Lagom website is a static website deployed to GitHub pages that is generated from this project.  This project uses twirl for templating, along with sbt-web to process web assets and webjar for dependencies.

The documentation is generated from the lagom main project as unstyled HTML snippets, along with javadocs/scaladocs, and an index file that describes the navigation structure of the documentation.  Each time the website is generated, the documentation pages are recreated from these HTML snippets, allowing the layout and styling to be kept up to date, and allowing links between versions of the documentation.

### Project layout

    - src
      - blog - each markdown file in here gets converted to a blog post
      - docs
        - 1.0.x - the documentation for each version of Lagom
          - index.json - the index for this version of the docs
          - api - api docs
      - main
        - markdown - each file in here gets served as a web page
        - twirl - twirl templates for the documentation
        - assets - compiled assets, eg, stylus stylesheets
        - public - static assets, eg images
        - scala - the Scala source code that actually does the documentation generation

### Developing the website

A static version of the website can be generated by running `sbt web-stage`.  This will output the entire website to the `target/web/stage` directory.  You should start a simple HTTP server from here to serve the docs, eg using `python -m SimpleHTTPServer`.  The `sbt ~web-stage` command can be used to tell sbt to watch for changes and rebuild the site whenever anything changes.

For convenience, `sbt run` does the above for you, starting a static Akka HTTP server in the stage directory, and then watching the filesystem for changes and rebuilding the site whenever it changes.

### Deploying the documentation

A new version of the markdown docs can be generated by running `sbt markdownGenerateAllDocumentation` in the `docs` directory of the main Lagom project, this outputs the documentation to `docs/target/markdown-generated-docs`.  This can be then copied into the corresponding `src/docs/<version>` directory in this project.

A new version of the API docs can be generated by running `sbt unidoc` in the main Lagom project.  This will output the javadocs in `target/javaunidoc`, which can then be copied into the corresponding `src/docs/<version>/api/java` directory in this project.

### Writing blog posts

Lagom uses a format similar to Jekyll for writing blog posts. Blog posts are written in markdown, prefixed with a front matter in YAML format. To create a new blog post, copy the `src/blog/_template.md` file to a new file in the same directory. The name of the file should match the URL that you want it served at, so for example, if you want it to be served at `blog/my-blog-post.html`, it should be in `src/blog/my-blog-post.md`.

Edit the front matter to suit your needs. The `title` and `date` fields are compulsory. The `author_github`, if provided, will be used by the website generator to lookup the author details from GitHub, including the name, URL and Avatar of the author. These can all be overridden, and must be provided manually if no GitHub username is provided, using the `author_name`, `author_url` and `author_avatar` fields.

The `tags` field can be used to specify a space separated list of tags. Multi-word tags can be configured by separating the words with a `+` symbol. The `summary` field can be used to provide a summary, this is what's displayed on the blog index page as well as on the tag index pages, and it will be markdown rendered. Usually it will simply be the first paragraph of the blog post.

The remainder of the file is the blog post. You can preview your blog post by following the [Developing the website](#developing-the-website) instructions above.


# Thanks

This project started as a fork of https://github.com/lagom/lagom.github.io

This project exists because [lagom site generator](https://github.com/lagom/lagom.github.io) project contains a lot of generated content: site and java docs and is focused on generating the site for lagom.

# Resources
- https://github.com/FlorianWolters/jekyll-bootstrap-theme/tree/master/_includes/themes/FlorianWolters
- https://github.com/pbinkley/jekyll-book
- https://github.com/vsch/flexmark-java
- https://jekyllrb.com/tutorials/hom\
