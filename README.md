Sekyll is a static site generator.

Is similar to jekyll but made in scala with minimum of WTF situations. Adheres to [POLA - Principle Of Least Astonishment](https://en.wikipedia.org/wiki/Principle_of_least_astonishment).


# Usage

## Generating only current page

- Start compilation process in background (compile changes in `.scala` and templates from `site/_layouts`)
   `sbt ~compile`
- Start http server that render only requested page on the fly. It will automatically FAST RELOAD only locally compiled classes (usually the templates). Best run it from your ide while the `~compile` is doing it's job.
   `run org.raisercostin.sekyll.SekyllSpringBootMainServer`
- Go to http://localhost:8080/

## Generating entire site

- To build entire static site to `public` folder
   `run org.raisercostin.sekyll.SekyllMain`

## Configuration

See `.sekyll.yml`.


# Guide

## How it works


### Items

An **Item** is rendered as a page.

The Item can be defined in several places:
- Any `.md` file contains in the begginging a `frontmatter` section ended with `---`. That `frontmatter` contains some data in `yaml` format.
- An item can have child items if it contains a `children` section in it's data.
- An item could be defined in the future in other ways: csv rows, sections inside freemind files

The Item has a `layout` property with the default value: `page`.
Other properties might be required by the layout templates defined in `site/_layouts` folder.

### Templates

A template is defined in `site/_layouts` folder: for example `blog.scala.html`.
A template could have a json schema constraint to define what properties are required.
Any Item that has a template with constrains will be validated before rendering.
The templating engine is twirl. The template is defined as a function that receives the Item parameter: `@(implicit item:Item)`
From item you can retrieve the followings:
- `@for(element <- item){ ... }` - iterate over sub-items.
- `data` - access all properties assigned to item
- `site` - access the entire site including all Items existent in site.

For example `blog.scala.json` is defined as:
```
{
    "title": "Blog",
    "type": "object",
    "properties": {
        "id": {
            "type": "string"
        },
        "date": {
            "type": "date-time"
        },
        "title": {
            "type": "string"
        }
        "summary": {
            "type": "string"
        }
        "author-name": {
            "type": "string"
        }
        "author-url": {
            "type": "string"
        }
        "author-avatar": {
            "type": "string"
        }
        "tags": {
            "type": "array",
            "items": {
              "type": "string"
            }
        }
    },
    "required": ["id","date","title","summary","author-name","author-url","author-avatar","tags"]
}
```

### Properties
- An item has many hierarchical properties accessible under `data`
- A property is hierarchic and can be access like `item.data.key1.key2.key3`
- A property can be converted in a special type if needed
  - `asString` - the default option
  - `asOptionString` - to get an Option[String] that can be used with `map`
  - `asUrl` - converts the value to a url relative to entire site taking into consideration the position of the folder where the item was found
  - `asOptionUrl`
  - `asOptionBoolean`
  - `iterator` if needed. Can also be used in template as `@for(myproperty <- item.data.key1.key2){ ...use myproperty.key3.key4.asUrl ... }`

## Adding resources
- Generate favicons from a square png at https://www.favicon-generator.org/ and put them in a favicons subfolder. See https://www.w3.org/2005/10/howto-favicon

  
## History

### 2018-08-25
- works better

### 2017-08-18
- upgrade to scala 2.12 (better integration with java 8 lambdas)

### 2017-09-16
- add yaml integration
  - https://en.wikipedia.org/wiki/YAML - nice samples
  - https://github.com/jcazevedo/moultingyaml
  - https://learnxinyminutes.com/docs/yaml/
  - https://bitbucket.org/asomov/snakeyaml/wiki/Documentation
- add import from freemind (.mm) files
  - http://freemind.sourceforge.net/wiki/index.php/Import_and_export
- add dev mode
  - https://stackoverflow.com/questions/3162457/how-to-check-with-javascript-if-connection-is-local-host

### 2017-08-29
- add sitemap generation

### 2017-08-26
- add logo
- add active menus
- add title
- generat favicons - http://www.favicon-generator.org/

### 2017-08-11
- more control of assetFingerPrint:
  1. manually changed in the `publicVersion` value in `build.sbt`? Drawback: browsers might cache styles and javascripts for long time and will not reload new versions. The generator should generate a new file from time to time or when the resources are changed.

# Backlog

## ToDo
- add GPDR - http://www.eugdpr.org/the-regulation.html
- The sample site generated in `master` branch: http://raisercostin.org/sekyll
- A second sample is at http://raisercostin.gitlab.io/sekyll
- list 400, 500 page errors at the end
- generate breadcrumb
- add i18n as in https://github.com/untra/polyglot (and for a list see https://github.com/Anthony-Gaudino/jekyll-multiple-languages-plugin)
- detect local broken links (from markdowns)
- add multiple source folders
- add dynamic filtering: https://vestride.github.io/Shuffle/
- publish at http://sekyll.gitlab.io and http://sekyll.github.io
- generate directly in `docs/` not in `target/web/stage`
- switch from style and sass to less as less has native compilation in sbt.
- cleanup
- deploy command to release static site to
  - folder
  - other branch
  - via travis
    - https://gist.github.com/domenic/ec8b0fc8ab45f39403dd
    - https://benlimmer.com/2013/12/26/automatically-publish-javadoc-to-gh-pages-with-travis-ci/
    - https://www.google.ro/search?q=github+publish+gitpages+after+travis
- render current branch by scripts in
  - github
  - gitlab (for private repos)
- yaml inheritance with default config
  - https://web.archive.org/web/20130213112648/http://blog.101ideas.cz/posts/dry-your-yaml-files.html
- publish via travis: https://github.com/jostly/template-sbt-travis
- swtich tags to semantic tags
  - https://coderwall.com/p/wixovg/bootstrap-without-all-the-debt
  - https://css-tricks.com/semantic-class-names/
  - https://stackoverflow.com/questions/22133639/bootstrap-and-html5-semantic-tags
  - https://stackoverflow.com/questions/23583235/how-to-combine-twitter-bootstrap-3-and-html5-semantic-elements
  - https://www.w3schools.com/html/html5_semantic_elements.asp
        Tag	Description
        <article>	Defines an article
        <aside>	Defines content aside from the page content
        <details>	Defines additional details that the user can view or hide
        <figcaption>	Defines a caption for a <figure> element
        <figure>	Specifies self-contained content, like illustrations, diagrams, photos, code listings, etc.
        <footer>	Defines a footer for a document or section
        <header>	Specifies a header for a document or section
        <main>	Specifies the main content of a document
        <mark>	Defines marked/highlighted text
        <nav>	Defines navigation links
        <section>	Defines a section in a document
        <summary>	Defines a visible heading for a <details> element
        <time>	Defines a date/time
- render first requested page and continue with others in background (to speedup dev lifecycle)
- add `@item.site.page` context info about current page. Two ways to implement it:
  - the iterator should modify this.
  - the Site is actually a Page that refers the site.  
- strict/lax evaluation of meta parameters 
- fnmatch (gitignore pattern format) implementation in scala 
  - https://gist.github.com/Aivean/6b2bb7c2473b4b7e1376fac1d2d49cf8
  - https://www.google.com/search?q=fnmatch+implemenataion+in+scala
- detect changed markdown files
- change markdown from pegdown to kramdown
- add mega menu https://bootsnipp.com/snippets/featured/bootstrap-mega-menu
- add markdown import/export - http://freemind.sourceforge.net/wiki/index.php/Import_and_export
- include pages from markdown
- iterators in markdown?
- semantic
  - https://nikcodes.com/2013/08/20/semantic-markdown/
  - https://blog.codinghorror.com/the-future-of-markdown/
  - http://markdown.github.io/
  - http://thenewcode.com/536/Adding-Phone-Numbers-To-Web-Pages-With-HTML5-and-Microdata
  - https://www.w3.org/TR/html5/common-idioms.html#conversations
  - http://microformats.org/wiki/hcard - hCard is a simple, open format for publishing people, companies, organizations on the web, using a 1:1 representation of vCard (RFC2426) properties and values in HTML. hCard is one of several open microformat standards suitable for embedding data in HTML/HTML5, and Atom/RSS/XHTML or other XML.
- content over form
  - https://www.crazyegg.com/blog/landing-page-essentials/
- check with web tools
  - pwa - Progressive Web App Checklist
    - https://developers.google.com/web/progressive-web-apps/
    - checklist & tools - https://developers.google.com/web/progressive-web-apps/checklist
  - AMP - Accelerated Mobile Pages 
    - https://en.m.wikipedia.org/wiki/Accelerated_Mobile_Pages
    - https://www.ampproject.org/
    - https://developers.google.com/amp/
    - amp html spec - https://www.ampproject.org/docs/fundamentals/spec
    - amp toolbox - https://github.com/ampproject/amp-toolbox
  - webmaster tools
    - https://www.google.com/webmasters/tools/mobile-usability?hl=en&siteUrl=http://www.dcsi.eu/#drilldown=CONTENT_NOT_SIZED_TO_VIEWPORT
    - https://developers.google.com/web/tools/lighthouse/audits/first-meaningful-paint
  - lighthouse
    - rec by https://developers.google.com/web/progressive-web-apps/checklist
    - extension https://chrome.google.com/webstore/detail/lighthouse/blipmdconlkpinefehnmjammfjpmpbjk
    - Quick-start guide on using Lighthouse:
      - http://bit.ly/lighthouse-quickstart 
      - https://docs.google.com/document/d/1pwLP9t8gKB3XyNKKz15dUPhWOeuHSqJ01paF9lN_y5g/edit
  - google
    - search console
      - https://www.google.com/webmasters/tools/site-message-view?hl=en&authuser=0&siteUrl=http://www.dcsi.eu
    - custom search 
      - https://cse.google.com/cse/create/new?hl=en&cselang=en&utm_source=wmx
  - favicon
    - https://realfavicongenerator.net/

## Bugs
- hover doesn't work on tablets/devices

## How could work
- Generate in a separate branch and publish from there.
  - There are some issues on keeping the history of that.
- Uploading only the sources and generate the public folder at runtime has the drawback of not having the history of the final generated site.

## Quircks
Here we explain what we tried and failed.

### Gitlab
- Don't publish the `target/web/stage` folder directory by configuring it in `.gitlab-ci.yml` as this will fail.
- Sass and Style sbt-web plugins need external dependency on nodejs/npm and it will take longer time to compile.
- Sbt fails to build in gitlab after 1h.

## Rejected
- more control of assetFingerPrint:
  - based on the git head: `val assetFingerPrint = "git rev-parse HEAD".!!.trim`
     - This has the problem that on any change anywhere all the files are affected.
  - based on the actual content of each generated/combined file instead of current version in head.
     - This is a fingerprinting on each file. Might work if a fingerprint on content on all of the files is computed.

# Resourcs

## Bootstrap templates/themes/style

- https://wrapbootstrap.com/theme/raleway-mega-bootstrap-template-WB09054T6
- https://startbootstrap.com/template-overviews/modern-business/


# Old

## Generating site locally

The build is made with `sbt`.
- eclipse: `sbt eclipse`
- compile: `sbt clean compile`
- generate web: `sbt webStage`
- start server for in dev mode with autorefresh: `sbt run` . Then go on http://localhost:8080/
- start server in debug mode `activator -jvm-debug 5005 "run 8080"`

The content is generated in `public/` folder.
Steps:
  - there are several types of files that will be statical types
    - **/*.md - markdon files converted to html
    - `src/main/twirl` - layout/tags pages that are compiled
       - any page can refer the entire content via `@item.site.xxx` properties
    - all other files - copied directly in docs folder

Everything in `public/` is published at raisercostin.gitlab.io .
The content in `public` is a manual copy of a `target/web/stage`.

Publish on
- github.com - See https://help.github.com/articles/configuring-a-publishing-source-for-github-pages/

features added
- types of urls - see https://www.digitalocean.com/community/tutorials/controlling-urls-and-links-in-jekyll for more explanations.
  - absolute: starting with `http://`, `https://` - These urls will not be changed at all.
  - root relative: starting with `/` - These urls will be prefixed with the configured context if specified.
  - relative: any other urls - These urls will be prefixed with configured context if specified and a relative path to the place that contains the content.
- serialize scala case classes in spring boot rest with jackson
- add scala config https://github.com/andr83/scalaconfig
  
