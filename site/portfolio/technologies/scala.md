---
title: Scala
titleHide: true
name: scala
image: https://d120jmftguczr4.cloudfront.net/img/why-scala/scala-logo.png
description: "Scala builds on top of java ecosystem. With it you can develop faster using a modern dynamic style while functional concepts like imutability enable more robust, concurent and parallel solutions. And all these are tightly integrated and reusable from any java solution."
layout: folder
---

# Scala

![](https://d120jmftguczr4.cloudfront.net/img/why-scala/scala-logo.png)