---
title: Integration & Data Migration
titleForced: true
#image: https://www.beyondtrust.com/wp-content/uploads/diagram-identity-access-management.png
layout: folderOnePage
action: more about integration ...
linkTo: /offer/integration
position: 3
cssClass: integration_data_migration
isLong: true
children:
  - item: share-data
    title: Discover Data
    image2: https://www.attunity.com/wp-content/uploads/2017/04/1-2-Mainframe.png
    description: |
      You have a knowledge treasure ready to be mined. Before that you need to integrate, migrate and share your existing knowledge and data with new systems. You have multiple options: extract, transform, load, consolidate, standardize, cleansing, reconcile it.
  - item: integrate-systems
    title: Integrate Systems
    image2: http://beyondplm.com/wp-content/uploads/2011/07/plm-data-share.png
    description: |
      The systems might show their ages: end of life of products, expiring licenses, unpatched security issues can be fixed with proxy systems or with an Enterprise Service Bus. This can also enable migration of legacy systems.
  - item: migrate-legacy-systems
    title: Migrate Legacy Systems
    image2: http://www.pipelinemarketing.com/wp-content/uploads/2016/05/Too-Busy.jpg
    metaphors: legacy systems technical debt, pile of crap, too busy to do busy work, chain weight, Prison Shackle, debt
    description: |
      When some legacy old systems are a technical debt to you: licensing costs too high, vendor lock-in, no security patches, end of life products, hard to find specialists you need to migrate them. Legacy systems can be migrated gradually if you have the knowledge on how to do it surgically. We've done that multiple times. Do it gradually while in total control of your systems.
description: |
  You already have production systems full of data. A new perspective might provide a boost. We worked with ESBs like WSO2 for a SOA architecture for enterprise application integration. We extracted data from databases like Oracle Database, Microsoft SQL Server, MySQL, MariaDB, Postgres; from external services via API: with xml centric technologies(ws/soap/xml/wsdl) but also with newer REST oriented APIs (swagger, JSON, YAML); from no-sql databases, files in java/scala using Spark. The migrated systems might be implemented in SOA, ESB but also as microservices to offer more flexibility.
---
