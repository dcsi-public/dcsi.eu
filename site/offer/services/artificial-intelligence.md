---
title: Artificial Intelligence
titleForced: true
description: |
    Scrap, crawl, extract, transform with AI: metadata, microdata, open graph, twitter cards and many more.
    The clustered data, automatic taxonomies will help.
    You don't need any manual work for this: just give us the website address or the domain. Forget about manual tunning of websites' specific crapy scrapers.
image: https://i.pinimg.com/736x/07/3e/75/073e750081760206230ba18a5aa24c81--free-website-monitor.jpg
layout: folderOnePage
action: more about AI ...
linkTo: /offer/artificial-intelligence
position: 10

---

## Why Artificial Intelligence?
- The companies are involved in a darwinian process: will survive the most adapted to business environment. Whoever takes the most informed decisions wins.
- Many companies have a lot of information but they can't seem to find them
- Now the big enterprises and even the startups can have access to structured data that ages ago was the privilege of the ones that could handle big bureaucratic systems: the big government institutions, religious institutions, aristocracy.

## Ce inseamna Inteligenta Artificiala?
- IA automatizeaza activitatile mentale in acelasi fel in care robotica automatizeaza activitatile fizice. Ambele sunt o solutie la muncile repetitive, grele si plictisitoare.
- IA incepe sa inlocuiasca sistemele software dedicate care codifica cunostintele si procesele in programe.  Se trece apoi la sisteme care externalizeaza si separa datele de program. Evoluand ajungem la sistemele de reguli: asa zisele sisteme expert (de genul celor de diagnostic medical). Ele deja pot fi catalogate ca sisteme simple IA. In ultimii ani doua tipuri de sisteme au inceput sa aiba succes extraordinar: Machine Learning si Deep Learning care imprumuta multe concepte din biologia si fiziologia neuronului.
- CEO Google spune ca Inteligenta Artificiala este probabil mai importanta decat descoperirea focului si electricitatii.
- IA va avea un impact enorm asupra societatii, exista o cursa a inarmarii intre state pe tema asta, exista avertizari si discutii intre mari fondatori de companii si oameni de stiinta pe tema eticii sistemelor IA. Ca si focul sau electricitatea tehnologia aceasta poate ajuta enorm dar si distruge enorm.

## Cum functioneaza
- Spre deosebire de sitemele clasice unui IA nu i se spune decat cum sa invete. Dupa asta este bombardat cu date care ii antreneaza baza de date/memoria/"neuronii"/tabula rasa. Dupa asta "amintirile" lui pot fi interogate fara ca el sa oboseasca sau sa fie plictisit de repetitia intrebarilor. Exista multiple tipuri de invatare: poate fi asistata sau neasistata, poate fi pe mai multe niveluri, poti antrena mintile intre ele daca poti creea un context de competitie intre minti: vezi recentul AlphaGo (jucator de GO al Google de nivel mondial)

## Ce oferim noi in domeniul asta
- Avem un extractor general de date (cu invatare nesupravegheata) din documente: darzar robot. RoBotul acesta nu trebuie rescris/readaptat/reprogramat pentru fiecare site sau tip de document pe care il proceseaza. Ce face el:
- **Extrage date primare** importante din surse variate de date: websites, calendare, documente, imagini  

- **Clasifica automat** (taxonomie/clustering) concepte si notiuni importante din datele extrase reusind de exemplu sa inteleaga urmatoarele:  

    - tipul de site: stiri, produse, servicii, prezentare  

    - tipul de pagina in site: de produs, about page, de contact, de lista de produse/servicii  

    - date specifice paginii: produse, evenimente, servicii, reclame, detalii de branding ce pot fi ignorate  

- **Browsing** - Ofera posibilitatea de a face browsing in datele descoperite. In felul acesta poti sa descoperi si sa intelegi cum sunt datele structurate. Un motor de cautare nu iti ofera rezultate decat daca stii exact ce cauti. De exemplu: daca cauti "rochie" intr-un sistem care a scanat produse: sistemul de browsing iti spune ca rochiile din sistem sunt scurte(2200), lungi(3400) si necunoscut(4320) pe cand google iti va da o serie de linkuri la rochii sperand ca tu sa gasesti ce vrei in primele 10 linkuri.  

- **Specialized UI** - O data ce ai un sistem prin care extragi date si ignori brandingul si detaliile "neesentiale" din alte site-uri/documente poti pune o interfata specializata peste ele axata pe utilizator. Poti avea o interfata pentru batrani, alta pentru copii, alta pentru oameni de afaceri, alta pentru tineri in functie de scenariile preferate de utilizare ale lor. Companiile deja au dashboarduri diferite in functie de rolurile persoanelor care acceseaza sistemele software.
- **Domenii, Industrii** - Pana acum am extras si procesat date din industrii diverse:
    - calendar evenimente din social media (facebook, linkedin, google calendar, anunturi evenimente publicatii locale)
    - anunturi din site-uri specializate pe servicii si vanzare produse
    - analiza sentiment din forumuri, facebook, twitter
    - stiri locale si internationale
    - date din site-uri de prezentare: orare, contact, organigrame
    - date personale interne fimelor pt a pregati GDPR: documente, baze de date
    - carti, muzica, prezentari, filme, mesaje, documente
    - adaptari pe site-uri de specialitate: tehnica dentara, fashion, electronice, jucarii

## Expertiza
- Concentrati pe solutii enterprise cu abordari din zona startup:
    - digitalizare - primul pas in folosirea informatiilor
    - integrare - al doilea pas in agregarea lor
    - big data, IoT - al treilea pas in acumularea de maxim de informatii
    - Analytics, Data Scientists - al patrulea pas in extragerea de informatii cu tehnici moderne
    - AI - al cincilea pas in extragerea de informatii automate
