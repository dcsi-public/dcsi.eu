---
title: Custom Software Development
titleHide: true
name: development
image: https://previews.123rf.com/images/olechowski/olechowski1311/olechowski131100029/23445564-Software-development-concept-in-tag-cloud-on-white-background-Stock-Photo.jpg
image2: https://www.illinoisworknet.com/DownloadPrint/Research_Development_Icon.png
layout: folder
description: ""
position: 11
---

Usually this is a Time & Materials project.
We work with the following technologies.