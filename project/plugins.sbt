addSbtPlugin("com.typesafe.sbt" % "sbt-twirl" % "1.3.4")
addSbtPlugin("com.typesafe.sbt" % "sbt-web" % "1.4.3")
//addSbtPlugin("com.typesafe.sbt" % "sbt-js-engine" % "1.2.2")

addSbtPlugin("net.ground5hark.sbt" % "sbt-concat" % "0.1.9")
addSbtPlugin("com.typesafe.sbt" % "sbt-uglify" % "2.0.0")
addSbtPlugin("com.typesafe.sbteclipse" % "sbteclipse-plugin" % "5.1.0")
addSbtPlugin("com.timushev.sbt" % "sbt-updates" % "0.3.4")
addSbtPlugin("net.virtual-void" % "sbt-dependency-graph" % "0.8.2")
addSbtPlugin("io.get-coursier" % "sbt-coursier" % "1.0.3")


resolvers += "Typesafe Releases Repository" at "http://repo.typesafe.com/typesafe/releases/"

// Used for our SimpleHTTPServer in Akka HTTP
libraryDependencies += "com.typesafe.akka" % "akka-http-experimental_2.10" % "2.0.4"
//libraryDependencies += "com.typesafe.akka" % "akka-http-experimental_2.11" % "2.4.11.2" //sbt still has 
