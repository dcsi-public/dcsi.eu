Sekyll is a static site generator made in scale based on jekyll (made in ruby).

# Intro

## Use it locally

- Install `sbt`.
- Run `sbt run`
- Go in browser to http://localhost:8080

## Configuration

Configuration file is `.sekyll.yml`.

# Concepts

## Markdown Page

A markdown page has extension `.md`. It might contain a data (FrontMatter) section written in yaml between `---` markers. Then it has a content section written in markdown language (kramdown dialect).

The markdown file is transformed to html and then is injected in the `layout` template page. The template might use other data from frontmatter. 

