package org.raisercostin.sekyll;

public class ErrorDetails{
    public final String path;
    public final String stacktrace;
    public ErrorDetails(String path, String stacktrace){
        this.path = path;
        this.stacktrace = stacktrace;
    }
}