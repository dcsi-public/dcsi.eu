package eu.dcsi.sekyll.docs

import java.io.InputStream
import java.net.URL
import java.nio.charset.Charset
import java.nio.charset.CodingErrorAction

import scala.collection.concurrent.TrieMap
import scala.io.Codec
import scala.io.Source

import play.api.Logger
import play.api.libs.json.Json
import play.api.libs.json.Reads
import org.raisercostin.syaml.Syaml
import org.raisercostin.syaml.Syaml
import org.raisercostin.syaml.StringSyamlSource
import org.raisercostin.jedi.InputLocation
import org.raisercostin.sekyll.Site
import org.raisercostin.syaml.Syaml
import org.raisercostin.namek.nodes.SNode
import org.raisercostin.namek.nodes.SyamlANode

object BlogMetaDataParser {
  //private implicit val codec = Codec(Charset.forName("US-ASCII"))

  private val gitHubCache = TrieMap.empty[String, GitHubUser]

  /**
   * Parse the front matter from a blog post.
   *
   * @param stream The stream to parse
   */
  def parsePostFrontMatter(stream: InputLocation): BlogPost = {
    val (yaml, markdown) = Site.extractFrontMatter(stream)
    toBlogPost(stream.baseName, yaml, markdown)
  }
  def toBlogPost(id: String, snode: SNode, markdown: String): BlogPost = {
    val yaml = snode.asInstanceOf[SyamlANode].syaml
    val title = yaml.get("title").asString.getOrElse {
      sys.error(s"No title specified in front matter of blog post $id")
    }
    val summary = yaml.get("summary").asString.getOrElse("")
    val date = yaml.get("date").asDate.getOrElse {
      sys.error(s"No date specified in front matter of blog post $id")
    }
    lazy val authorGithub = yaml.get("author_github").asString.map(getGitHubUser)
    val authorName = yaml.get("author_name").asString orElse {
      authorGithub.map(_.name)
    } getOrElse {
      sys.error(s"No author_name or author_github in front matter of blog post $id")
    }
    val authorUrl = yaml.get("author_url").asString orElse {
      authorGithub.map(_.html_url)
    } getOrElse {
      sys.error(s"No author_url or author_github in front matter of blog post $id")
    }
    val authorAvatar = yaml.get("author_avatar").asString orElse {
      authorGithub.map(_.avatar_url)
    } getOrElse {
      sys.error(s"No author_avatar or author_github in front matter of blog post $id")
    }

    val tags = yaml.get("tags").asString.toOption.toSeq.flatMap(_.split(" +").map(_.replace('+', ' ')))

    BlogPost(id, date, markdown, title, summary, BlogAuthor(authorName, authorUrl, authorAvatar), tags.toSet)
  }


  private def getGitHubUser(name: String): GitHubUser = {
    gitHubCache.getOrElseUpdate(name, {
      try {
        val stream = new URL(s"https://api.github.com/users/$name").openStream()
        try {
          Json.parse(stream).as[GitHubUser]
        } finally {
          stream.close()
        }
      } catch {
        case e: java.io.IOException => { //if DocumentationGenerator.devMode => {
          // GitHub might be rate-limiting us. If we're in development mode, just ignore this.
          Logger.warn(s"GitHub might be rate-limiting us; using fallback for user $name")
          GitHubUser(name, "https://www.gravatar.com/avatar/default", s"https://github.com/$name")
        }
      }
    })
  }
}

case class GitHubUser(name: String, avatar_url: String, html_url: String)

object GitHubUser {
  implicit val reads: Reads[GitHubUser] = Json.reads[GitHubUser]
}

