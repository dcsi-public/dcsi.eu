package eu.dcsi.sekyll.docs

import org.joda.time.DateTime

import scala.xml.{Elem, PCData}
import org.joda.time.format.ISODateTimeFormat
import play.twirl.api.Html
import org.raisercostin.sekyll.Site
import org.raisercostin.sekyll.Item
import play.utils.UriEncoding

package object html {
  def encodePathSegment(url: String): String = {
    UriEncoding.encodePathSegment(url, "utf-8")
  }

}
object FeedFormatter {
  def atom(posts: Seq[(BlogPost, Html)])(implicit item:Item): Elem = {
    val blogUpdate = ISODateTimeFormat.dateTime.print(posts.head._1.date)
    val blogUrl = item.site.baseUrl + "/blog"
    val year = DateTime.now().getYear

    <feed xmlns="http://www.w3.org/2005/Atom" xmlns:dc="http://purl.org/dc/elements/1.1/">
      <title>Lagom Blog</title>
      <link rel="alternate" type="text/html" href={blogUrl} />
      <link rel="self" type="application/atom+xml" href={blogUrl + "/atom.xml"} />
      <id>{blogUrl}</id>
      <rights>Copyright {year}, Lightbend</rights>
      <updated>{blogUpdate}</updated>
      <dc:creator>Lightbend</dc:creator>
      <dc:date>{blogUpdate}</dc:date>
      <dc:language>en</dc:language>
      <dc:rights>Copyright {year}, Lightbend</dc:rights>
      {posts.map {
      case (post, content) =>
        val postDate = ISODateTimeFormat.dateTime.print(post.date)
        val postUrl = s"$blogUrl/${post.id}.html"

        <entry>
          <title>{post.title}</title>
          <link rel="alternate" href={postUrl} />
          {post.tags.map { tag =>
            <category term={tag} scheme={s"$blogUrl/tags/${html.encodePathSegment(tag)}.html"} />
        }}
          <author>
            <name>{post.author.name}</name>
            <uri>{post.author.url}</uri>
          </author>
          <id>{postUrl}</id>
          <updated>{postDate}</updated>
          <published>{postDate}</published>
          <content type="html">{PCData(makeAbsoluteLinks(content.body, item.site.baseUrl, item.site.baseUrl))}</content>
          <dc:date>{postDate}</dc:date>
        </entry>
    }}
    </feed>
  }

  def makeAbsoluteLinks(body: String, rootUrl: String, relativeUrl: String): String = {
    val body1 = body
    val body2 = rootRelativelinkFinder.replaceAllIn(body1, "$1" + rootUrl + "/")
    val body3 = relativeLinkFinder.replaceAllIn(body2, "$1" + relativeUrl + "/")
    val body4 = rootRelativeImgFinder.replaceAllIn(body3, "$1" + rootUrl + "/")
    val body5 = relativeImgFinder.replaceAllIn(body4, "$1" + relativeUrl + "/")
    body5
  }

  val rootRelativelinkFinder = """(<a [^>]*href=")/(?![/"])""".r
  val relativeLinkFinder = """(<a [^>]*href=")(?!(http://|https://|/))""".r
  val rootRelativeImgFinder = """(<img [^>]*src=")/(?![/"])""".r
  val relativeImgFinder = """(<img [^>]*src=")(?!(http://|https://|/))""".r
}
