package org.raisercostin.sekyll

import org.raisercostin.jedi.{AbsoluteBaseLocation, NavigableFileInputLocation, NavigableLocation}
import org.raisercostin.jedi.impl.JediFileSystem

import scala.util.Success
import scala.util.Try

import org.raisercostin.jedi.FileLocation
import org.raisercostin.jedi.Locations
import org.raisercostin.jedi.RelativeLocation
import org.raisercostin.syaml.Syaml
import eu.dcsi.sekyll.docs.FeedFormatter
import eu.dcsi.sekyll.docs.Sitemap
import eu.dcsi.sekyll.docs.SitemapUrl
import play.twirl.api.Html
import org.raisercostin.nodes.freemind.FreeMindAsObject


import org.raisercostin.syaml.SyamlEmpty

import org.joda.time.DateTime
import scala.util.Failure
import org.raisercostin.syaml.SyamlList
import org.raisercostin.syaml.StringSyamlSource
import org.raisercostin.syaml.FromParentSyaml
import org.joda.time.Duration
import org.raisercostin.jedi.InOutLocation
import org.raisercostin.jedi.NavigableInOutLocation
import org.raisercostin.jedi.InputLocation
import play.twirl.api.BaseScalaTemplate
import com.google.common.cache.CacheBuilder
import java.util.concurrent.TimeUnit
import com.google.common.cache.CacheLoader
import scala.util.control.NonFatal
import rx.lang.scala.Observable
import org.raisercostin.jedi.FileAltered
import org.raisercostin.namek.nodes.SNode
import org.raisercostin.namek.nodes.SNodes
import org.raisercostin.namek.nodes.SyamlANode
import org.raisercostin.namek.nodes.YamlNodeValidator
import org.raisercostin.namek.nodes.JsonNodeValidator
import org.raisercostin.jedi.NavigableInputLocation
import org.apache.commons.lang3.ObjectUtils
import org.apache.commons.lang3.exception.ExceptionUtils
import com.google.common.cache.LoadingCache
import java.lang.reflect.Method


import scala.language.dynamics
import org.jsoup.Jsoup
import org.jsoup.safety.Whitelist

class SNodeWrapper(val item: Item, val snode: SNode) extends Dynamic with Iterable[SNodeWrapper]{
  override def isEmpty: Boolean = snode.isEmpty
  override def iterator: Iterator[SNodeWrapper] = asIterable.iterator

  def asUrl: String = asOptionUrl.get
  def asOptionUrl: Option[String] = asOptionString.map{image=>
    if (MarkdownItem.isAbsolute(image))
      image
    else
      item.baseUrl + "/" + image
  }
  def asOptionString: Option[String] = snode.asOptionString
  def asOptionBoolean: Option[Boolean] = snode.asOptionBoolean
  def asString: String = snode.asString
  def asIterable: Iterable[SNodeWrapper] = snode.asIterable.map(new SNodeWrapper(item, _))

  def asOption: Option[SNodeWrapper] = {
    if (snode.isEmpty)
      None
    else
      Some(this)
  }
  def child(key: String) = selectDynamic(key)
  def selectDynamic(key: String):SNodeWrapper = new SNodeWrapper(item, snode.selectDynamic(key))
  def addChild(key:String,value:Any):SNodeWrapper = new SNodeWrapper(item, snode.addChild(key,value))


  import scala.reflect.runtime.universe._
  def as[T](implicit tag: WeakTypeTag[T]): T = snode.as[T]
}

trait Item extends Iterable[Item]{
  self =>
  override def iterator: Iterator[Item] = children.toIterator


  /** Needed to compute baseUrl. Location of the item. */
  def location: NavigableInputLocation

  /** Content with relative address should be rendered relative to this baseUrl. */
  def baseUrl: String = site.computeBase(location)

  def rawdata: SNode

  def data: SNodeWrapper = new SNodeWrapper(self, rawdata)

  final lazy val syaml: Syaml = {
    val s = rawdata.asInstanceOf[SyamlANode].syaml
    val layoutName = extractLayout(rawdata)
    val layoutSchema = Locations.file(site.config.source).child("_layouts/" + layoutName + ".schema.json")
    if (layoutSchema.exists)
      rawdata.validate(YamlNodeValidator.from(JsonNodeValidator.fromLocation(layoutSchema)))
    s
  }

  def extractLayout(s: SNode): String = s.layout.asOptionString.getOrElse("default")

  def getLayout: Try[String] = Try {
    extractLayout(rawdata)
  }

  //lazy val syaml: Syaml = SyamlEmpty
  import sys.process._

  lazy val rawSys: Syaml = Syaml.parse(
    s"""
system :
  moment :
    timestamp : "${(new DateTime).toString("yyyy-MM-dd-HH-mm-ss")}"
    date: "${(new DateTime).toString("yyyy-MM-dd")}"
    time: "${(new DateTime).toString("HH-mm-ss")}"
  version: unknown
  revision: ${"git rev-parse HEAD".!!}
  build:
    user: ${System.getProperty("user.name")}
    email: unknown
""")(StringSyamlSource("BuildIdentification"))

  def id: String = slug

  def title: Option[String]

  def shortTitle: Option[String] = title

  def titleIfNeeded: Option[String] = None

  def image: String

  def fullImage: String

  def fullImageOption: Option[String] = Option(fullImage).filter(_.nonEmpty)

  def rendered: Html

  def link: String = slug

  def slug: String

  final def description: String = data.description.asString

  def classSelectedPage(selectedSlug: String): String =
    if (isActive(selectedSlug))
      site.classSelectedPage
    else
      ""

  import scala.reflect.runtime.universe._

  def position: Int = getOr(data, "position", Int.MaxValue)

  def getOr[T](node: SNodeWrapper, key: String, value: => T)(implicit tag: WeakTypeTag[T]): T = node.child(key).as[Option[T]].getOrElse(value)

  def isActive(selectedSlug: String): Boolean = {
    //println(s"page $slug <> $selectedSlug")
    if (selectedSlug.endsWith("*"))
      slug.startsWith(selectedSlug.stripSuffix("*"))
    else
      slug == selectedSlug
  }

  def canonical: Option[String] = None

  def site: Site

  def children: Seq[Item]
}

case class ErrorItem(item: Item, file: FileLocation, why: String, name: String, error2: Throwable, site: Site) extends Item {
  override def location: NavigableInputLocation = item.location //item.map(_.location).getOrElse(site.sourceLocation)
  lazy val rawdata: SNode = SyamlANode(SyamlEmpty)

  def title: Option[String] = Some(name)

  def image: String = ???

  def fullImage: String = ???

  def rendered: Html = ???

  def error: String = ExceptionUtils.getStackTrace(error2)

  override def slug: String = name //slug2//site.rootRelative(name) //Attention! affects how the classSelectedPage is defined
  def children: Seq[Item] = Seq()
}

case class MarkdownFileChildItem(file: FileLocation, site: Site, parent: SNodeWrapper, x: SNodeWrapper, index: Int) extends MarkdownItem {
  override def location: NavigableInputLocation = file

  def rawdata: SNode = data.snode
  override lazy val data: SNodeWrapper = {
    val z = x.asIterable
    //println(z.zipWithIndex.mkString("\n"))
    //println("size=" + x.asIterable.size)
    val node =
    if (x.size <= 1)
      x.head
    else
      x

    val newNode = node.addChild("position", index).addChild("parent", s"children/$index") //s"${parent.source} - children/$index")
    newNode
  }
  override val rendered = Html("")
}

case class SimpleMarkdownFileItem(file: FileLocation, site: Site) extends MarkdownItem {
  override def location: NavigableInputLocation = file

  lazy val (rawdata: SNode, markdown: String) = //site.timed(s"readYamlMarkdown from ${file.absolute}")(
  Site.extractFrontMatter(file)

  lazy val rendered: Html = {
    logger.info(s"render                  ${file.raw}")
    val renderedPost = Html(site.markdownToHtml(markdown))
    renderedPost
  }
}

case class MarkdownFileItem(file: FileLocation, site: Site) extends MarkdownItem {
  override def location: NavigableInputLocation = file

  private var other: SimpleMarkdownFileItem = null
  private var lastModificationTime: String = null

  private def update: SimpleMarkdownFileItem = {
    if (isChanged) {
      init2()
    }
    other
  }

  private def init2():Unit = {
    lastModificationTime = file.versionFromModificationTime
    other = SimpleMarkdownFileItem(file, site)
  }

  def isChanged: Boolean =
    file.versionFromModificationTime != lastModificationTime

  def rawdata: SNode = update.rawdata

  def markdown: String = update.markdown

  def rendered: Html = {
    val html = update.rendered
    val fixedLinks = FeedFormatter.makeAbsoluteLinks(html.body, site.baseUrl, baseUrl)
    Html(fixedLinks)
  }

}

object MarkdownItem {
  def isAbsolute(image: String) = image.startsWith("http://") || image.startsWith("https://") || image.startsWith("://") || image.isEmpty
}

trait MarkdownItem extends Item with Slf4jLogger {
  def file: FileLocation

  def site: Site

  def rendered: Html

  def relative(relativePath: String): RelativeLocation = file.parent.child(relativePath).extractPrefix(Locations.file(site.config.source)).get

  def title: Option[String] = data.title.asOptionString

  override def shortTitle: Option[String] = data.shortTitle.asOptionString.orElse(title)

  override def titleIfNeeded: Option[String] = if (titleIsNeeded) title else None

  def titleIsNeeded: Boolean = {
    val op = data.child("titleForced").asOptionBoolean
    op.
      orElse(
        data.child("titleHide").
          asOptionBoolean.
          map(!_)).getOrElse(true)
  }

  def image: String = data.child("image").asOptionString.getOrElse("")

  def fullImage: String = image //if (isAbsolute(image)) image else slugForChildren + "/" + image

  lazy val recursive: Boolean = data.child("recursive").asOptionBoolean.getOrElse(false)

  def contains(item: MarkdownFileItem): Boolean = {
    val parent = site.slug2(this.file)
    val child = site.relativeFile(item.file)
    val contains =
      if (recursive)
        item != this && child.ancestor(parent) == parent
      else
        item != this && child.parent == parent
    contains
  }

  override def slug: String = site.rootRelative(site.slug2(this.file).relativePath)

  def slugForChildren: String =
    if (file.baseName == "index")
      site.slug2(this.file).relativePath
    else
      site.slug2(this.file).parent.relativePath

  def children: Seq[MarkdownItem] = childrenFromYaml.toSeq ++ site.markdownFileItems.filter(contains).sortBy(_.position)

  lazy val childrenFromYaml: Iterable[MarkdownFileChildItem] = {
    val childrenSyaml = data.child("children").asIterable
    childrenSyaml.zipWithIndex.map {
      case (x, index) =>
        MarkdownFileChildItem(file, site, data, x, index)
    }
  }
}

case class SiteConfig(yaml: SNode) extends Slf4jLogger {
  val baseUrl = yaml.baseUrl.asString
  //getString("baseUrl")
  val source = yaml.source.asString
  //yaml.getString("source")
  val destination = yaml.destination.asOptionString.getOrElse("target\\unconfiguredDefaultSekyllDestination") //yaml.getString("destination")
  val exclude = yaml.exclude.map(_.asString) //as[List[String]]//asList[String]//get("exclude").asList[String]
  logger.info(s"""loaded config \n---\n${yaml.toString}\n...""") //yaml.dump
}

object Site extends Slf4jLogger {
  val config = SiteConfig(readYaml().get)

  val observeFiles: Observable[FileAltered] = Locations.file(config.source).watch(1000)
  observeFiles.doOnEach(file => println(s"$file was changed"), error => logger.warn("Watching files error", error), () => logger.info("Watching files completed."))
  //observeFiles.
  //observeFiles.
  //, x => println(s"$x was changed"))

  def readYaml() = {
    val defaultSekyll = Locations.classpath("default.sekyll.yml")
    val sekyll = Locations.file(".sekyll.yml")
    val yamlContent = defaultSekyll.readContent + "\n" + sekyll.readContent

    {
      val effective = Locations.file("target/config-composed.sekyll.yml")
      effective.mkdirOnParentIfNecessary.writeContent(yamlContent)
      logger.info(s"save config composed at $effective")
    }

    val yaml = SNodes.parseYamlViaSyaml(yamlContent)
    //val yaml = Syaml.parse(yamlContent)(StringSyamlSource(s"From $defaultSekyll and $sekyll"))
    val finalYaml = Locations.file("target/config-final.sekyll.yml")
    logger.info(s"save config final    at $finalYaml")
    Try {
      //finalYaml.writeContent(yaml.dump)
      finalYaml.writeContent(yaml.toString)
    }.get
    yaml
  }

  val menu = FreeMindAsObject.load(Locations.file(config.yaml.data.asString))

  def apply(): Site = Site(
    config.yaml.outputDir.asString,
    config.yaml.assetFingerPrint.asString)

  def apply(outputDir: String, assetFingerPrint: String, devMode: Boolean=true): Site =
    Site(Locations.file(outputDir), assetFingerPrint, devMode)

  //TODO implement
  //- https://gist.github.com/Aivean/6b2bb7c2473b4b7e1376fac1d2d49cf8
  //- https://www.google.com/search?q=fnmatch+implemenataion+in+scala
  def accept(file: RelativeLocation): Boolean = {
    val path = file.relativePath
    config.exclude.forall { x: String => !path.contains(x) }
  }

  def extractFrontMatter(stream: InputLocation): (SNode, String) = {
    logger.info(s"extractFrontMatter from ${stream.raw}")
    implicit class IteratorOps[T](i: Iterator[T]) {
      def nextOption = if (i.hasNext) Option(i.next()) else None
    }

    val lines: Iterator[String] = stream.readLines.iterator.dropWhile(_.isEmpty)
    // Extract attributes from the front matter
    lines.nextOption match {
      case Some(start) if start.trim == "---" =>
        val (frontMatterLines, contentLines) = lines.span(_.trim != "---")
        val frontMatter = frontMatterLines.mkString("\n")
        val content = contentLines.drop(1).mkString("\n")
        (SNodes.loadYamlViaSyaml(Locations.memory(s"FrontMatterLocation[$stream]").writeContent(frontMatter)).get /*(StringSyamlSource(s"FrontMatterLocation[$stream]"))*/ , content)
      case Some(x) =>
        (SNodes.loadYamlViaSyaml(Locations.memory(s"EmptyFrontMatterLocation[$stream]").writeContent("")).get /*(StringSyamlSource(s"FrontMatterLocation[$stream]"))*/ , lines.mkString("\n"))
    }
  }
}

/**
  * The site gets passed to every page.
  * Is a wrapper around the generic RawSite making the mapping to final domain types.
  */
case class Site(outputDir: FileLocation, assetFingerPrint: String, devMode: Boolean) extends Slf4jLogger {
  val config = Site.config
  val data: SNode = config.yaml
  val src = Locations.file(config.source)
  val markdownDir = Locations.file(config.yaml.markdownDir.asString)
  val blogDir = Locations.file(config.yaml.blogDir.asString)


  def classSelectedPage = "active"

  def title = config.yaml.title.asString

  def route(image: String) = routeImage(image)

  // Set this to Some("your-github-account-name") if you want to deploy the docs to the gh-pages of your own fork
  // of the repo
  //val gitHubAccount: Option[String] = None
  //for master
  //val (baseUrl, context) = ("http://raisercostin.org/sekyll","/sekyll")
  //for template
  //val (baseUrl, context) = ("http://localhost:8080","")
  //gitHubAccount match {
  //case Some(account) => (s"http://$account.github.io/lagom.github.io", "/lagom.github.io")
  //case None => ("https://www.lagomframework.com", "")
  //}
  def baseUrl: String = config.baseUrl

  @deprecated
  def path: String = baseUrl

  @deprecated
  def context: String = baseUrl

  private def routeImage(image: String): String = s"images/$image"

  import scala.language.dynamics

  class SiteLayout extends Dynamic {
    def selectDynamic(layoutName: String): Seq[MarkdownFileItem] = markdownFileItems.filter(_.data.layout.asOptionString.getOrElse("") == layoutName)
  }

  val items = new SiteLayout

  def item(collection: String): Item =
    markdownFileItems.find(_.data.title.asOptionString.getOrElse("") == collection).getOrElse(throw new IllegalArgumentException(s"Cannot find collection [$collection]"))

  def items(collection: String): Seq[Item] = documents[Item](collection)

  def documents[T](collection: String): Seq[T] = {
    val relativeCollection = rootRelative(collection)
    markdownFileItems.find(_.slug == relativeCollection).getOrElse(throw new IllegalArgumentException(s"Collection [$collection] is not defined. Existing collections are [${markdownFileItems.map(_.slug)}]")).children.asInstanceOf[Seq[T]]
  }

  def isPartner(doc: MarkdownFileItem) = doc.file.path.contains("/partners/")

  val (markdowns: Iterable[FileLocation], files: Iterable[FileLocation]) =
    src.descendants.filter(f => Site.accept(f.extractPrefix(src).get)).filter(!_.name.startsWith("_")).partition(_.extension == "md")

  lazy val markdownFileItems: Seq[MarkdownFileItem] = //timed("analyze collections") {
    markdowns.map { file =>
      MarkdownFileItem(file, this)
      //}
    }.toSeq
  //}.value

  lazy val renderer = new MarkdownRenderer()

  def markdownToHtml(markdown: String) = renderer.markdownToHtml(markdown)

  case class RenderingTemplate(name: String) {
    lazy val method = {
      val clazz = Class.forName("html." + name);
      //assumed you have a String parameter for your template
      val render: java.lang.reflect.Method = //clazz.getDeclaredMethod("render", classOf[Item])
        clazz.getDeclaredMethods.filter(_.getName.equals("render")).head
      render
      //logger.warn("warning", new IllegalArgumentException(s"File ${item.file} uses unknown layout [$x]. Using default [page]"))
    }

    def file = {
      //val clazz = Class.forName("html." + name);
      Locations.classpath("html/" + name + ".class")
    }
  }

  lazy val cachedData: LoadingCache[String, RenderingTemplate] = CacheBuilder.newBuilder()
    .maximumSize(20)
    .build(
      new CacheLoader[String, RenderingTemplate]() {
        def load(key: String) = RenderingTemplate(key)
      }
    )

  def layout(layoutTemplate: RenderingTemplate, item: Item): Try[Html] = Try {
    //val method = cachedData.get(name)
    if (!layoutTemplate.method.getParameters.apply(0).getType.isAssignableFrom(item.getClass))
      throw new RuntimeException(s"Layout [${layoutTemplate.name}] needs a parameter of type [${layoutTemplate.method.getParameters.apply(0).getType}] but got a value of type [${item.getClass}].");
    val html = layoutTemplate.method.invoke(null, item).asInstanceOf[Html]
    html
  }.recoverWith {
    case e: java.lang.reflect.InvocationTargetException =>
      Failure(new RuntimeException(s"Error when rendering with template [${layoutTemplate.name}] the item [$item]: " + e.getCause.getMessage, e.getCause))
  }

  def toOutputWithLayout(item: MarkdownFileItem): Page = {
    val res = new Page(item)
    res
  }

  def slug2(file: FileLocation): RelativeLocation = {
    relativeFile(file).withExtension(_ => "")
  }

  def relativeFile(file: FileLocation): RelativeLocation = {
    val f = file.extractPrefix(src).get
    if (f.name == "index.md")
      f.parent
    else
      f
  }
  class Page(item: MarkdownFileItem) {
    val why = s"markdown page ${item.file}"
    val name =
      item.data.slug.asOptionString.getOrElse(
        item.site.relativeFile(item.file).withExtension(_ => "").relativePath)

    val sitemapPriority = "0.8"
    val includeInSitemap: Boolean = true

    /*This must be a function. Otherwise the rendering is done fast and you will get circular dependencies between Site.sitemap and Site.generated fields.*/
    lazy val rendered: Try[Any] = render()

    val finalName = if (Locations.file(name).extension.isEmpty)
      name.stripSuffix("/") + "/index.html"
    else
      name
    val sitemapUrl = finalName match {
      case "/index.html" => ""
      case path if path.endsWith("/index.html") => path.stripSuffix("/index.html")
      case other => other
    }
    val file = outputDir.child(finalName.stripPrefix("/"))
    Site.logger.info(f"prepareSavePage [${file.toPath}%-100s] reason: $why")


    private def render(): Try[Any] = Try {
      val itemMoment = item.file.versionFromModificationTime
      val fileMoment = file.existingOption.map(_.versionFromModificationTime)

      lazy val layoutName = item.getLayout match {
        case Success("default") =>
          "page"
        case Failure(x) =>
          logger.warn("warning", new IllegalArgumentException(s"File ${item.file} uses unknown layout [$x]. Using default [page]", x))
          "page"
        case Success(x) =>
          x
      }
      lazy val layoutTemplate: RenderingTemplate = cachedData.get(layoutName)
      lazy val templateMoment = layoutTemplate.file.versionFromModificationTime

      val shouldRenderAgain = fileMoment.map(x => itemMoment > x || templateMoment > x || item.site.shouldRenderAgain).getOrElse(true)

      if (shouldRenderAgain) {
        val page: Try[Html] = layout(layoutTemplate, item)
        val result = page.map { r =>
          //file.getParentFile.mkdirs()
          logger.info(f"savePage [${file.toPath}%-100s] reason: $why")
          file.mkdirOnParentIfNecessary.writeContent(cleanup(r.body))
          //Files.write(file.toPath, rendered.body.getBytes("utf-8"))
        }.recover {
          case x =>
            Try {
              logger.info(f"savePageWithErrors [${file.toPath}%-100s] reason: $why")
              file.mkdirOnParentIfNecessary.writeContent(layout(cachedData.get("page500"), ErrorItem(item, file, why, name, x, Site.this)).recover[Html] { case NonFatal(x) => new Html(ExceptionUtils.getStackTrace(x)) }.get.body)
              //logger.info(f"deletePage [${file.toPath}%-100s] reason: ${x.getMessage} if is not folder(isFile=${file.isFile})")
              //if (file.isFile) file.deleteIfExists
            }.get
            throw x
        }
        result
      } else {
        logger.info(f"already savedPage [${file.toPath}%-100s] reason: $why")
        //file.readContent
        Success({})
      }
    }
    
    def cleanup(html:String):String = {
      html.replaceAll("[ ]{4}|[ ]{3}\t","\t").replaceAll("\t","    ").replaceAll("\\s*(\n|\r\n)","\n")
      //Jsoup.clean(html, Whitelist.relaxed())
      //Jsoup.parse(html, "", org.jsoup.parser.Parser.xmlParser()).toString()
    }
  }

  //  private def getNav(ctx: Context, acc: List[Section] = Nil): List[Section] = {
  //    ctx.parent match {
  //      case None => acc
  //      case Some(parent) =>
  //        val titles = parent.children map { case (t, u) => NavLink(t, u, t == ctx.title) }
  //        val url = parent.children.head._2
  //        getNav(parent, Section(parent.title, url, titles) :: acc)
  //    }
  //  }

  //
  //  lazy val (blogPosts, blogPostsByTag, blogSummary) = {
  //    val blogPosts = Blog.findBlogPosts(blogDir.toFile)
  //    val blogPostTags = blogPosts.flatMap(_.tags).distinct.sorted
  //    val blogPostsByTag = blogPostTags.map(tag => tag -> blogPosts.filter(_.tags.contains(tag)))
  //    val blogSummary = {
  //      BlogSummary(blogPosts.take(3), blogPostsByTag.map {
  //        case (tag, posts) => tag -> posts.size
  //      })
  //    }
  //    (blogPosts, blogPostsByTag, blogSummary)
  //  }
  //
  //  lazy val allPages: Seq[Page] = {
  //    val blogPostSummaries = blogPosts.map { post =>
  //      post -> Html(markdownToHtml(post.summary))
  //    }
  //
  //    implicit val item = PageItem("dummyBlogCommonItem", Site.this)
  //    val renderedBlogPosts = blogPosts.map { post =>
  //      val renderedPost = Html(markdownToHtml(post.markdown))
  //      post -> renderedPost
  //    }

  //    renderedBlogPosts.map {
  //      case (post, renderedPost) =>
  //        val fixedLinks = if (baseUrl.nonEmpty) FeedFormatter.makeAbsoluteLinks(renderedPost, baseUrl, baseUrl) else renderedPost
  //        new SimplePage(s"blogPost ${post.id}", s"blog/${post.id}.html", _ => Try { eu.dcsi.website.part2.html.blogPost(post, fixedLinks) }, sitemapPriority = "0.8")
  //    } ++
  //    blogPostsByTag.map {
  //      // Tag pages
  //      case (tag, posts) =>
  //        val postSummaries = posts.flatMap { post =>
  //          blogPostSummaries.find(_._1.id == post.id)
  //        }
  //        new SimplePage(s"blogPost tag=$tag", s"blog/tags/$tag.html", _ => Try { html.blog(s"Blog posts tagged with $tag", renderRecent = true, postSummaries) })
  //    } :+
  //    {
  //      // Index page
  //      new SimplePage(s"blogPostIndex2", "blog2/index.html", _ => Try { html.blog("Blog2", renderRecent = false, blogPostSummaries.take(10)) },
  //        sitemapPriority = "0.5")
  //    } :+
  //    {
  //      new SimplePage(s"blogPostIndex", "blog/index.html", _ => Try { eu.dcsi.website.html.blog("Blog", renderRecent = false, blogPostSummaries.take(10))(item) },
  //        sitemapPriority = "0.5")
  //    } :+ {
  //      new CustomWritePage(s"atom.xml generated through code","blog/atom.xml",
  //          (this2: OutputFile) => Try {
  //          XML.save(this2.file.absolute, FeedFormatter.atom(renderedBlogPosts.take(10)), enc = "utf-8")
  //        },includeInSitemap = false, sitemapPriority = "")
  //      // Feed
  //      Try {
  //        OutputFile(outputDir, "blog/atom.xml", "atom.xml generated through code", None, includeInSitemap = false, sitemapPriority = "", (this2: OutputFile) => Try {
  //          XML.save(this2.file.absolute, FeedFormatter.atom(renderedBlogPosts.take(10)), enc = "utf-8")
  //        })
  //    }
  //  }

  //  lazy val allBlogOutputFiles: Seq[Try[OutputFile]] = allPages.map(_.toOutputFile)

  lazy val generatedPages: Seq[Page] = markdownFileItems.map {
    toOutputWithLayout
  }
  lazy val generated: Seq[Page] = {
    val phase1 = //pages.map((generatePage2 _).tupled) ++
    //renderMarkdownFiles("", markdownDir.toFile) ++
      generatedPages.map {
        x => {
          x.rendered.map(_ => x)
        }
      }
    // ++
    //allBlogOutputFiles ++
    //redirects.map((generateRedirect _).tupled)
    val all = phase1.partition {
      _.isSuccess
    }
    all._2.collect {
      case Failure(error) =>
        logger.error("error", error)
    }
    all._1.collect { case Success(x) => x }
  }

  def sitemap: Seq[String] = generatedPages.map(x => x.sitemapUrl)

  case class Timed[T](msg: String, start: DateTime, end: DateTime, value: T, duration: Duration)

  @inline def timed[T](msg: String)(action: => T): Timed[T] = {
    val start = new DateTime()
    logger.info(s"$msg: start at $start")
    val value = action
    val end = new DateTime()
    val duration = new Duration(start, end)
    logger.info(s"$msg: finished at $end in ${duration}")
    Timed(msg, start, end, value, duration)
  }


  lazy val renderAll = RenderAll()

  def loadMarkdownItem(localPath: String, exactMatch: Boolean = false): Seq[MarkdownFileItem] = {
    val page2 = src.child(localPath).nameAndBefore
    markdownFileItems.filter { x =>
      logger.trace(s"search for [${page2}] in [${x.file.nameAndBefore}]");
      if (exactMatch)
        x.slug.stripPrefix("/") == localPath
      else
        x.file.nameAndBefore.contains(page2)
    }
  }

  def loadPage(localPath: String, exactMatch: Boolean = false): Seq[Page] = timed(s"renderPage $localPath") {
    loadPage2(localPath, exactMatch)
  }.value

  private def loadPage2(localPath: String, exactMatch: Boolean = false): Seq[Page] =
    loadMarkdownItem(localPath, exactMatch).map(toOutputWithLayout)

  def renderPage(localPath: String): Seq[Try[Any]] =
    loadPage2(localPath, true)
      .map(x => timed(s"render $localPath") {
        x.rendered
      }.value)

  case class RenderAll() {

    val time = timed("renderAll") {
      val duplicates = generated.groupBy(_.file).filter(_._2.size > 1)
      if (duplicates.nonEmpty)
        logger.info(
          s"""
    !!!! Attention

    Duplicate pages found:
    ${duplicates.mkString("\n")}
    
    """)

      // sitemaps
      val mainSitemap = Sitemap("sitemap-main.xml", generated.filter(_.includeInSitemap)
        .map(file => SitemapUrl(file.sitemapUrl, file.sitemapPriority)))

      val generatedSitemaps: Seq[InOutLocation] = Sitemap.generateSitemaps(outputDir, baseUrl, Seq(mainSitemap))
      val includeErrors = config.yaml.renderPagesForErrors.asOptionBoolean.getOrElse(true)

      val generatedSet: Set[InOutLocation] =
        (if (includeErrors)
          generated
        else
          generated.filter(_.rendered.isSuccess)).map(_.file).toSet ++ generatedSitemaps
      //
      //      def cleanOldFiles(file: NavigableInOutLocation): Unit = {
      //        if (file.isFolder) {
      //          file.list.foreach(cleanOldFiles _)
      //          if (file.list.isEmpty) {
      //            file.delete
      //          }
      //        } else {
      //          if (!generatedSet.contains(file)) {
      //            logger.info(s"deleteOldFile: $file")
      //            file.delete
      //          }
      //        }
      //      }

      logger.info(s"*******\nrender")
      val results = generated.map(x => (x -> x.rendered))


      //TODO replace with one call from latest jedi-io release
      /** Return pairs of files that has the same relative names under src and dest. Each location can be tested for existence with `exists` or existingOption. */
      def compare[T1 <: NavigableLocation, T2 <: NavigableLocation](src: T1, dest: T2): Observable[Tuple2[T1, T2]] = {
        val a = src.descendants.map { x =>
          //emit (some,some) and (some,none)
          Tuple2(x, dest.child(x.extractPrefix(src).get))
        } ++
          //emit (none,some)
          (dest.descendants.map { x =>
            Tuple2(src.child(x.extractPrefix(dest).get), x)
          }.filter(!_._1.exists))
        Observable.from(a)
      }

      val ignore = config.yaml

      def explain(what: String, res: org.raisercostin.jedi.NavigableLocation) = logger.info(f"""$what%20s ${res.absolute}""")

      val dest = Locations.file(config.destination).mkdirIfNecessary //.backupExistingOne
      val c = compare(src.mkdirIfNecessary, dest.mkdirIfNecessary)//.filter{case Tuple2(s,d)=>Site.accept(s.extractPrefix(src).get)&&Site.accept(d.extractPrefix(dest).get)}
      c.foreach {
        case (s,d) if !Site.accept(s.extractPrefix(src).get) =>
          explain("ignore source", s)
        case (s,d) if !Site.accept(d.extractPrefix(dest).get) =>
          explain("ignore dest", d)
        case (s, d) if !s.exists =>
          if (d.isFolder) {
            //println(s"ignore folder $d")
          } else if (generatedSet.contains(d))
            explain("keep generated", d)
          else {
            val assets = Seq("fonts", "css", "js", "lib", "images")
            val relative = d.extractPrefix(dest).get.nameAndBefore
            if (!assets.contains(JediFileSystem.splitPartialPath(relative)(0))) {
              explain("delete old", d)
              d.delete
            } else {
              explain("keep asset", d)
            }
          }
        case (s, d) if !d.exists =>
          explain("should copy", s)
          d.mkdirOnParentIfNecessary.copyFrom(s)
        case (s, d) =>
          explain("keep copied", d)
      }
      //println("compare:::")
      //c.foreach(println)

      //logger.info(s"cleanup ${outputDir.toFile}")
      //cleanOldFiles(outputDir)

      //      val copyAll = timed("copy non-markdown") {
      //        files.toSeq.map { file =>
      //          val child = file.extractPrefix(src).get
      //          logger.info(s"copy $child to $dest")
      //          dest.child(child).mkdirOnParentIfNecessary.copyFrom(file)
      //        }
      //      }

      logger.info(s"*******\nerrors")
      results.filter(_._2.isFailure).foreach { x => logger.info(s"error for ${x._1}", x._2.failed.get); /* x._2.failed.get.printStackTrace*/}
    }
  }

  def computeBase(file: NavigableInputLocation): String = {
    val base = file.extractPrefix(sourceLocation).get.parent.relativePath
    if (baseUrl.nonEmpty)
      baseUrl + "/" + base
    else
      base
  }

  lazy val sourceLocation = Locations.file(config.source)

  def rootRelative(path: String): String = "/" + path

  //TODO always render everything as pages depend on site that can be changed by any page
  def shouldRenderAgain: Boolean = true
}