package org.raisercostin.sekyll

import java.io.FileNotFoundException
import java.nio.file.{Files, Paths}

import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import javax.servlet.http.HttpServletRequest
import org.raisercostin.jedi.{FileChanged, Locations}
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.{EnableAutoConfiguration, SpringBootApplication}
import org.springframework.boot.{CommandLineRunner, SpringApplication}
import org.springframework.context.annotation._
import org.springframework.data.rest.webmvc.ResourceNotFoundException
import org.springframework.http.{HttpHeaders, ResponseEntity}
import org.springframework.web.bind.annotation.{RequestMapping, RequestMethod, RestController}
import play.Logger
import reactor.core.publisher.{Flux => JFlux, Mono => JMono}
import reactor.core.scala.publisher.{Flux, Mono}

import scala.util.{Failure, Try}

object SekyllSpringBootMainServer {
  def main(args: Array[String]): Unit = {
    SpringApplication.run(classOf[SekyllSpringBootApp], args: _*)
  }
}

//import com.revomatico.util.TypesafePropertySourceFactory
//see on how to configure https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-external-config.html
//@SpringBootApplication(scanBasePackages = Array("org.raisercostin.sekyll"))
//@PropertySources(// @PropertySource("classpath:/restfs.properties")
//  Array(new PropertySource(Array("classpath:/restfs.conf"))))
//,@PropertySource("classpath:/restfs-test.conf")
// uncommenting will delete the database
// , @PropertySource("classpath:/restfs-costin.properties")
// ,@PropertySource(factory=TypesafePropertySourceFactory.class, value="classpath:/restfs-dev2.conf")
// ,@PropertySource(factory=TypesafePropertySourceFactory.class, value="classpath:/restfs-dev5.conf")
//))
@SpringBootApplication(scanBasePackages = Array("org.raisercostin.sekyll"))
@PropertySources(Array(new PropertySource(factory = classOf[TypesafePropertySourceFactory], value = Array("classpath:/restfs.conf"))))
class SekyllSpringBootApp extends CommandLineRunner {
  @throws[Exception]
  override def run(args: String*): Unit = {

  }
}


object SekyllController {
  private val logger = org.slf4j.LoggerFactory.getLogger(classOf[SekyllController])
  val site = Site()
}

case class FileStatus(var files: Seq[String])

//case class ErrorDetails2(val path: String, val stacktrace: String)

@RestController
class SekyllController {

  import SekyllController._

  @Value("#{'${restfs.folders}'.split(',')}")
  //@Value("${restfs.folders}")
  val places: java.util.List[String] = null

  @RequestMapping(path = Array("/**"), method = Array(RequestMethod.GET))
  def getOrList(request: HttpServletRequest): JMono[ResponseEntity[Object]] = {
    val localPath = extractFilePath(request)
    logger.info("request {}", localPath)
    import scala.collection.JavaConverters._
    val a: Mono[ResponseEntity[Object]] =
      Flux.fromIterable(places.asScala)
        .map { parent: String => logger.debug("check in parent {}", parent); Paths.get(parent) }
        .map(x => pathForRemainingUri(x, localPath))
        .map { x => site.renderPage(localPath).collect{case Failure(y)=>y}.foreach(z=>logger.warn(s"Couldn't render $localPath",z)); x }
        .map { childPath =>
          val child = childPath.toString
          logger.debug("check file {}", child);
          if (Files.isDirectory(childPath)) {
            childPath.resolve("index.html")
          } else {
            childPath
          }
        }
        .filter(Files.exists(_))
        .map { childPath =>
          logger.info("get {}", childPath)
          download(childPath)
        }
        .next

    a.switchIfEmpty(Mono.error(new ResourceNotFoundException(s"Resource $localPath not found in $places"))).asJava
    //a.asJava
  }

  import java.io.IOException
  import java.nio.file.{Files, Path, Paths}

  import org.springframework.core.io.ByteArrayResource
  import org.springframework.http.{MediaType, ResponseEntity}

  private def pathForRemainingUri(parent: Path, localPath: String) = pathForRequestPath(parent, localPath)

  private def pathForRequestPath(parent: Path, destination2: String) = {
    val destination =
      if (destination2.startsWith("/")) destination2.substring(1)
      else if (destination2.startsWith("\\")) destination2.substring(1)
      else destination2
    val folder = destination
    val childPath = parent.resolve(folder).toAbsolutePath
    childPath
  }

  // @RequestMapping(path = "/restfs/v1/**", method = RequestMethod.GET)
  private def download(filePath: Path): ResponseEntity[Object] = try {
    val file = filePath.toFile
    val path = Paths.get(file.getAbsolutePath)
    val resource = new ByteArrayResource(Files.readAllBytes(path))
    // get the mimetype
    //    var mimeType = URLConnection.guessContentTypeFromName(file.getName)
    //    if (mimeType == null) mimeType = "application/octet-stream"
    val mimeType: String = Locations.file(file).mimeType.map(_.mimeType).getOrElse("application/octet-stream")
    val headers = new HttpHeaders()
    headers.add("Content-Disposition", String.format("inline; filename=\"" + file.getName + "\""))
    // headers.add("Content-Disposition", String.format("attachment; filename=\"" + file.getName() + "\""));
    ResponseEntity.ok.headers(headers).contentLength(file.length).contentType(MediaType.parseMediaType(mimeType)).body(resource)
  } catch {
    case e: IOException =>
      throw new RuntimeException(e)
  }

  import org.springframework.util.AntPathMatcher

  private def extractFilePath(request: HttpServletRequest) = {
    val path = request.getAttribute(org.springframework.web.servlet.HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE).asInstanceOf[String]
    val bestMatchPattern = request.getAttribute(org.springframework.web.servlet.HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE).asInstanceOf[String]
    val apm = new AntPathMatcher
    apm.extractPathWithinPattern(bestMatchPattern, path)
  }

  import java.io.IOException
  import java.nio.file.{Files, Paths}
  import java.util

  private def fileList(directory: String): Seq[String] = {
    val fileNames = new util.ArrayList[String]
    val directoryStream = Files.newDirectoryStream(Paths.get(directory))
    try {
      import scala.collection.JavaConversions._
      for (path <- directoryStream) {
        fileNames.add(path.toFile.getName)
      }
    } catch {
      case e: IOException =>
        throw new RuntimeException(e)
    } finally if (directoryStream != null) directoryStream.close()
    import scala.collection.JavaConverters._
    fileNames.asScala
  }

  import java.nio.file.InvalidPathException

  import org.springframework.http.{HttpStatus, ResponseEntity}
  import org.springframework.web.bind.annotation.ExceptionHandler

  import scala.tools.jline_embedded.internal.Log

  @ExceptionHandler(Array(classOf[RuntimeException], classOf[InvalidPathException]))
  //@RequestMapping(Array("${server.error.path:${error.path:/error}}"))
  def handleError(exception: RuntimeException, webRequest: HttpServletRequest): ResponseEntity[ErrorDetails] = {
    Log.warn("handleError", exception)
    // return new ResponseEntity<>(new ErrorDetails(Throwables.getStackTraceAsString(exception)), HttpStatus.NOT_FOUND);
    // return ResponseEntity.status(HttpStatus.NOT_FOUND).contentType(MediaType.TEXT_PLAIN).body(Throwables.getStackTraceAsString(exception));
    new ResponseEntity[ErrorDetails](new ErrorDetails(webRequest.getServletPath, exception.getMessage), HttpStatus.NOT_FOUND)
    //new ResponseEntity[RuntimeException](exception, HttpStatus.NOT_FOUND)
  }

  @ExceptionHandler(Array(classOf[ResourceNotFoundException]))
  //@RequestMapping(Array("${server.error.path:${error.path:/error}}"))
  def handleFileNotFound(exception: ResourceNotFoundException, webRequest: HttpServletRequest): ResponseEntity[ErrorDetails] = {
    Log.warn("File not found: " + exception.getMessage)
    Log.debug("File not found: ", exception)
    // return new ResponseEntity<>(new ErrorDetails(Throwables.getStackTraceAsString(exception)), HttpStatus.NOT_FOUND);
    // return ResponseEntity.status(HttpStatus.NOT_FOUND).contentType(MediaType.TEXT_PLAIN).body(Throwables.getStackTraceAsString(exception));
    new ResponseEntity[ErrorDetails](new ErrorDetails(webRequest.getServletPath, exception.getMessage), HttpStatus.NOT_FOUND)
    //new ResponseEntity[RuntimeException](exception, HttpStatus.NOT_FOUND)
  }
}

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.MapperFeature
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule

@Configuration
class JacksonConfiguration {
  @Bean
  def objectMapper: ObjectMapper = {
    val mapper = new ObjectMapper
    println("configure scala jackson serialization")
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
    mapper.configure(MapperFeature.DEFAULT_VIEW_INCLUSION, true)
    mapper
  }
}

//@Configuration
//@EnableAutoConfiguration
//@ComponentScan
//class SampleConfig {
//  @Bean
//  @Primary
//  def objectMapper(): ObjectMapper = {
//    println("configure scala jackson serialization")
//    val objectMapper = new ObjectMapper with ScalaObjectMapper
//    objectMapper.registerModule(DefaultScalaModule)
//    objectMapper
//  }
//}

import com.typesafe.config.Config
import org.springframework.core.env.PropertySource


class TypesafeConfigPropertySource(name: String, source: Config) extends PropertySource[Config](name, source) {
  override def getProperty(path: String): Object = {
    if (path.contains("[")) return null
    if (path.contains(":")) return null
    if (source.hasPath(path)) return source.getAnyRef(path)
    null
  }
}

import java.io.IOException

import com.typesafe.config.{ConfigFactory, ConfigParseOptions, ConfigResolveOptions}
import org.springframework.core.env.PropertySource
import org.springframework.core.io.Resource
import org.springframework.core.io.support.{EncodedResource, PropertySourceFactory}


object TypesafePropertySourceFactory {
  private def getNameForResource(resource: Resource) = {
    "TypesafePropertySource@" + resource.getFilename
    //String name = resource.getDescription();
    //if (!StringUtils.hasText(name)) {
    //String name = resource.getClass().getSimpleName() + "@" + System.identityHashCode(resource);
    //}
    //return name;
  }
}

class TypesafePropertySourceFactory extends PropertySourceFactory {
  @throws[IOException]
  override def createPropertySource(name: String, resource: EncodedResource): PropertySource[_] = {
    val config = ConfigFactory.load(resource.getResource.getFilename, ConfigParseOptions.defaults.setAllowMissing(false), ConfigResolveOptions.noSystem).resolve
    val safeName = if (name == null) TypesafePropertySourceFactory.getNameForResource(resource.getResource)
    else name
    new TypesafeConfigPropertySource(safeName, config)
  }
}

// System.out.println("Active profiles: " + Arrays.toString(ctxt.getEnvironment().getActiveProfiles()));
object SekyllMainServer {
  def main(args: Array[String]): Unit = {
    SimpleHTTPServer(new java.io.File("public"), 8080)
  }
}

object SekyllMain {
  def main(args: Array[String]): Unit = {
    //println(Site().renderPage("""C:\data\work2\dcsi\dcsi-site\site\offer\new\gdpr""").map(_.get.render.get).head)
    //println(Site().renderPage("""C:\data\work2\dcsi\dcsi-site\site\offer\solutions""").map(_.get.render.get).head)
    Site().renderAll.time.duration
    //watch()
    //Site().renderPage("""offer\new\gdpr\""").map(_.render)
    //Site().renderPage("portfolio\\technologies\\java\\").map(_.render)

  }

  def watch() = {
    Locations.file("site").watch(1000).map { x =>
      x match {
        case e: FileChanged =>
          println(e)
          Site().loadPage(e.location.name).map(_.rendered)
      }
    }.toBlocking.last
  }
}

object SekyllMainFromBuild {
  def main(args: Array[String]): Unit = {
    val devMode = sys.props.contains("dev")
    if (devMode) Logger.info("Running in dev mode")
    else Logger.info("Running in prod mode")
    //  val outputDir = new File(args(0))
    //  val docsDir = new File(args(1))
    //  val markdownDir = new File(args(2))
    //val blogDir = new File(args(3))

    val site = Try {
      Site(outputDir = args(0), assetFingerPrint = args(1),devMode).renderAll
    }.get
  }
}