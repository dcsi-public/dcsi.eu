package org.raisercostin.sekyll

import org.slf4j.Logger
import org.slf4j.LoggerFactory

trait Slf4jLogger { self =>
  val logger: org.slf4j.Logger = LoggerFactory.getLogger(self.getClass)
}