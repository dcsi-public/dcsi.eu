package org.raisercostin.sekyll

import org.junit.Assert._
import org.junit.Test
import org.raisercostin.jedi.Locations
import org.raisercostin.syaml.Syaml
import com.typesafe.config.Config
import com.typesafe.config.ConfigFactory
import com.typesafe.config.impl.ConfigParser
import org.raisercostin.nodes.freemind.FreeMind
import org.raisercostin.nodes.freemind.FreeMindAsObject
import com.typesafe.config.ConfigUtil
import com.typesafe.config.ConfigRenderOptions
import com.typesafe.config.ConfigObject
import com.typesafe.config.impl.SimpleConfigObject
import scala.util.Try
import com.typesafe.config.ConfigList
import org.raisercostin.nodes.freemind.MindMap
import scala.xml.Elem
import org.junit.Ignore

class PageRenderTest {
  @Test def acceptFile() {
    assertEquals(false, Site.accept(Locations.relative("""./project/target/node-modules/webjars/npm/node_modules/wrappy/README.md""")))
  }
  @Test def loadMarkdownItem() {
    val site = Site()
    val page = site.loadMarkdownItem("""portfolio/technologies/java""").head
    println(s"dump\n---\n${page.syaml.dump}")
    assertEquals(8, page.syaml.size)
    val child = page.syaml.sections
    println(child)
    assertEquals(8, child.children.size)
    //assertEquals(false, Site.accept(Locations.relative("""./project/target/node-modules/webjars/npm/node_modules/wrappy/README.md""")))
    println(s"dump\n---\n${site.toOutputWithLayout(page).rendered}")
  }
  @Test def loadMarkdownItem2() {
    val site = Site()
    val page = site.loadMarkdownItem("""offer\index""").head
    println(s"dump\n---\n${page.syaml.dump}")
    //assertEquals(8,page.syaml.size)
    //val child = page.syaml.sections
    //println(child)
    //assertEquals(2,child.children.size)
    //assertEquals(false, Site.accept(Locations.relative("""./project/target/node-modules/webjars/npm/node_modules/wrappy/README.md""")))
    println(s"dump\n---\n${site.toOutputWithLayout(page).rendered}")
  }
  @Test def renderPage() {
    val site = Site()
    val page = site.loadPage("""offer\iam""").map { x => x.rendered; x }
    println(page)
    //assertEquals(false, Site.accept(Locations.relative("""./project/target/node-modules/webjars/npm/node_modules/wrappy/README.md""")))
  }
  @Test def renderPageAndDownloadImages() {
    val site = Site()
    val page = site.loadPage("""offer\iam\iga\evolveum""").map { x => x.rendered; x }
    println(page)
    //assertEquals(false, Site.accept(Locations.relative("""./project/target/node-modules/webjars/npm/node_modules/wrappy/README.md""")))
  }
}